import matplotlib.pyplot as plt
import json
from matplotlib import rcParams

def main():
    with open("stats.json", 'r') as f:
        contents = f.read()

    stats = json.loads(contents)
    language_times = {}

    for run in stats:
        for language, elapsed in run.items():
            if language not in language_times:
                language_times[language] = []
            language_times[language].append(elapsed)

    averages = {}
    for language, times in language_times.items():
        count = 0
        sum = 0
        for time in times:
            sum += time
            count += 1
        averages[language] = sum / count
        
    stats = sorted(averages.items(), key=lambda x: x[1])
    languages = []
    elapsed_times = []
    for language, elapsed in stats:
        languages.append(language)
        elapsed_times.append(elapsed)
    
    rcParams['font.family'] = 'Avenir' # On Mac.
    fig, ax = plt.subplots()

    width = 0.35
    ax.bar(languages, elapsed_times, width)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.set_ylabel('Niceness')
    ax.set_title('Language niceness for Project Euler problems 1-10')

    plt.savefig('results.png', dpi = 300)



if __name__ == "__main__":
    main()