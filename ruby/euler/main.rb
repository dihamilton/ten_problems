
require_relative 'problems/problem1'
require_relative 'problems/problem2'
require_relative 'problems/problem3'
require_relative 'problems/problem4'
require_relative 'problems/problem5'
require_relative 'problems/problem6'
require_relative 'problems/problem7'
require_relative 'problems/problem8'
require_relative 'problems/problem9'
require_relative 'problems/problem10'


def main
    problems = [Problem1.new(), Problem2.new(), Problem3.new(), Problem4.new(), Problem5.new(), Problem6.new(), Problem7.new(), Problem8.new(), Problem9.new(), Problem10.new()]

    total_elapsed_milliseconds = 0
    problems.each do |problem|
        problem.run
        problem.report
        total_elapsed_milliseconds += problem.elapsed_milliseconds
    end

    puts('Ruby total elapsed: ' + total_elapsed_milliseconds.to_s + 'ms')
end

main