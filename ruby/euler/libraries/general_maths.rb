
def to_digits(n)
    digits = []
    temp = n
    rem = 0
    while temp > 0 do
        rem = temp % 10
        digits.append(rem)
        temp /= 10     # division in python 3 defaults to floating point, use // syntax to do integer division instead
    end

    return digits
end

def is_palindrome_array(digits)
    size = digits.length()
    halfway = size / 2     # division in python 3 defaults to floating point, use // syntax to do integer division instead
    for i in 0..halfway
        if digits[i] != digits[(size - 1) - i]
            return false
        end
    end
    return true
end

def is_palindrome(n)
    return is_palindrome_array(to_digits(n))
end