

# Sieve of Eratosthenes
# https:#en.wikipedia.org/wiki/Sieve_of_Eratosthenes
def sieve(n)

    integers = Array.new(n, false)  # Create an n sized array all containing false
    primes = []

    # Create a list of odd integers from 3 up, because we already know 2 is the only even prime

    (3..n).step(2) do |i|
        integers[i] = true # All these integers are possibly prime
    end

    # Since we're not including it above, let's add 2 to our list of known primes
    primes.push(2)

    p = 3
    multipleOfP = 0
    while p*p <= n do    # We can stop looking when p² is greater than n

        multipleOfP = p*p  # Start from p² since smaller multiples of p will have already been marked
                            # e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                            # and even numbers have already been discounted

        # Strike out the multiples of p all the way up to n
        while multipleOfP <= n do
            integers[multipleOfP] = false  # Since this number has at least one factor - p, it's not prime
            multipleOfP += 2*p     # Go up in steps of 2p to avoid even numbers which are already out of the list
                                    # e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        end
        
        # Find the next number that isn't struck off 
        while p <= n do
            p += 2
            if integers[p] == true
                break
            end
        end
    end
        
    for i in 0..n
        if integers[i]
            primes.push(i)
        end
    end
        
    return primes
end

def prime_factorise(n, max_expected_factor)

    primes = sieve(max_expected_factor)

    factors = {}        # Prime factors and how many times they occur
    factor = 0

    limit = n/2

    primes.each do |prime|

        if prime > limit
            break
        end
        factor = prime

        # If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        # our prime divide the candidate so we can add it again.
        # E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        # and so on
        while n % factor == 0 do
            if !factors.include?(prime)
                factors[prime] = 0
            end
            factors[prime] += 1
            factor *= prime
        end
    end
    if factors.length() == 0
        factors[n] = 1
    end

    return factors
end


def nth_prime(n)

    # Value of the nth prime is approximately n*ln(n)
    m = 2 * n
    approx_value = m * Math.log(m)
    # Generate sieve up to this value
    primes = sieve(approx_value)
    return primes[n-1]
end
