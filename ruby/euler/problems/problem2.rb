require_relative 'problem'

class Problem2 < Problem
    def initialize
        super(2, 'Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million.')
    end

    def solve
        a = 1
        b = 2
        c = 0
        sum = 2  # Already seen one even number

        while c < 1000000 do
            c = a + b
            if c % 2 == 0
                sum += c
            end
            a = b
            b = c
        end

        return sum
    end
end