require_relative 'problem'

class Problem9 < Problem
    def initialize
        super(9, 'Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.')
    end

    def solve
        product = 0
        c = 0
        a = 1
        b = 1
        found = false

        for a in 1..500
            for b in 1..500
                c = Math.sqrt((a*a) + (b*b))
                if a + b + c == 1000
                    found = true
                    break
                end
            end

            if found
                break
            end
        end

        product = a * b * c.to_i  

        return product
    end
end