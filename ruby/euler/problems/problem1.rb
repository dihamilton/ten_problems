require_relative 'problem'

class Problem1 < Problem
    def initialize
        super(1, 'Add all the natural numbers below 1000 that are multiples of 3 or 5.')
    end

    def solve
        limit = 1000
        sum = 0 # sum of all the multiples

        for i in 0..(limit - 1) do
            if i % 3 == 0 or i % 5 == 0
                sum += i
            end
        end
        return sum
    end
end