require_relative 'problem'

class Problem8 < Problem
    def initialize
        super(8, 'Find the greatest product of five consecutive digits in the 1000-digit number.')
    end

    def solve
        file = File.open("/data/8.txt")
        n = file.read
        file.close

        # Here we could use the property that the numeric value of a digit char is the ascii value minus 48 but this seems more obvious
        digitMap = { '0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9 }
        
        largestProduct = 0
        product = 0

        length = n.length
        # Go through and calculate all the 5 consecutive digit products in the number and return the largest found
        for i in 0..(length - 5)
            product = digitMap[n[i]] * digitMap[n[i+1]] * digitMap[n[i+2]] * digitMap[n[i+3]] * digitMap[n[i+4]]

            if product > largestProduct
                largestProduct = product
            end
        end
        return largestProduct
    end
end