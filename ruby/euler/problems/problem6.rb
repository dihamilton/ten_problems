require_relative 'problem'

class Problem6 < Problem
    def initialize
        super(6, 'What is the difference between the sum of the squares and the square of the sums?')
    end

    def solve
        difference = 0
        num = 100

        sumOfSquares = 0
        squareOfSum = 0

        for i in 1..num
            sumOfSquares += i*i
            squareOfSum += i
        end

        squareOfSum *= squareOfSum

        difference = squareOfSum - sumOfSquares

        return difference
    end
end