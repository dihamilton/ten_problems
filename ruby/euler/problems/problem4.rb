require_relative 'problem'
require_relative '../libraries/general_maths'

class Problem4 < Problem
    def initialize
        super(4, 'Find the largest palindrome made from the product of two 3-digit numbers.')
    end

    def solve
        low = 100
        high = 999
        largest_palindrome = 0

        high.step(low, -1) do |i|
            990.step(low, -11) do |j|
                num = i * j
                if is_palindrome(num)
                    if num > largest_palindrome
                        largest_palindrome = num
                    end
                end
            end
        end
                
        return largest_palindrome
    end
end