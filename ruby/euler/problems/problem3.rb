require_relative 'problem'
require_relative '../libraries/primes'

class Problem3 < Problem
    def initialize
        super(3, 'Find the largest prime factor of a composite number.')
    end

    def solve
        num = 600851475143
        factors = prime_factorise(num, 10000)
        # Since we identify factors from smallest to largest we can use the end of the list
        return factors.keys.last
    end
end