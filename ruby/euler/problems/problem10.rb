require_relative 'problem'
require_relative '../libraries/primes'

class Problem10 < Problem
    def initialize
        super(10, 'Calculate the sum of all the primes below one million.')
    end

    def solve
        # Get all the primes below one million
        primes_array = sieve(1000000)
            
        # Sum them up
        sum = 0
        primes_array.each do |prime|
            sum += prime
        end

        return sum
    end
end