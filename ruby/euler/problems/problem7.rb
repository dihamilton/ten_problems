require_relative 'problem'
require_relative '../libraries/primes'

class Problem7 < Problem
    def initialize
        super(7, 'Find the 10001st prime.')
    end

    def solve
        return nth_prime(10001)
    end
end