

class Problem
    def initialize(number, description)
        @number = number
        @description = description
        @answer = 0
        @elapsed_milliseconds = 0
    end

    def number
        return @number
    end

    def description
        return @description
    end

    def answer
        return @answer
    end

    def elapsed_milliseconds
        return @elapsed_milliseconds
    end
        
    def run
        start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
        @answer = solve()
        stop = Process.clock_gettime(Process::CLOCK_MONOTONIC)
        elapsed_seconds = stop - start
        @elapsed_milliseconds = elapsed_seconds * 1000
    end

    def report
        puts "#{@number}. #{@description}"
        puts "Answer: #{@answer} in #{@elapsed_milliseconds.round(6)} ms"
    end

    # Child classes should override solve
    # Note there is no specific language construct for enforcing it to be
    # implemented but polymorphism will still work
    def solve
        raise 'Should be implemented by child class'
    end

end