#ifndef TEN_PROBLEMS_PROBLEM4_H
#define TEN_PROBLEMS_PROBLEM4_H


#include "problem.h"

class Problem4 : public Problem
{
public:
    
    Problem4();

    int64_t solve();

};


#endif