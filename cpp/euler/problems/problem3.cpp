#include "problem3.h"
#include "libraries/primes.h"

using namespace euler::primes;

Problem3::Problem3() 
 : Problem(3, "Find the largest prime factor of a composite number.")
 {
 }

 int64_t Problem3::solve()
 {
    int64_t num = 600851475143;
    auto factors = prime_factorise(num, 10000);
    return (--factors.end())->first;
 }