#ifndef TEN_PROBLEMS_PROBLEM1_H
#define TEN_PROBLEMS_PROBLEM1_H


#include "problem.h"

class Problem1 : public Problem
{
public:
    
    Problem1();

    int64_t solve();

    int64_t chat_gpt_solve();

};


#endif