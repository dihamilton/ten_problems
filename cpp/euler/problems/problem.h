#ifndef TEN_PROBLEMS_PROBLEM_H
#define TEN_PROBLEMS_PROBLEM_H

#include <string>

class Problem
{
public:
    
    Problem(int number, const std::string &description);
    virtual ~Problem() {};
    int number();
    std::string description();
    double elapsed_milliseconds();
    int answer();
    void run();
    void report(bool redact_answers);

protected:
    // Child classes should override
    virtual int64_t solve() = 0;
    virtual int64_t chat_gpt_solve(){return 0;};

private:
    int m_number = 0;
    std::string m_description;
    int64_t m_answer = 0;
    double m_elapsed_milliseconds = 0.0;
    int64_t m_chat_gpt_answer = 0;
    double m_chat_gpt_elapsed_milliseconds = 0.0;
};


#endif