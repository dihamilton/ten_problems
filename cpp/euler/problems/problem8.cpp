#include "problem8.h"
#include <fstream>
#include <iostream>
#include <string>
#include <map>

using namespace std;

Problem8::Problem8() 
 : Problem(8, "Find the greatest product of five consecutive digits in the 1000-digit number.")
 {
 }


 int64_t Problem8::solve()
 {
    ifstream in("/data/8.txt");
    if(!in.is_open()) throw runtime_error("couldn't load 1000 digit number");
    string n;
    in >> n;

    // Here we could use the property that the numeric value of a digit char is the ascii value minus 48 but this seems more obvious
    map<char, int> digit_map = { {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4}, {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9} };
    
    int largest_product = 0;
    int product = 0;

    int length = static_cast<int>(n.length());
    // Go through and calculate all the 5 consecutive digit products in the number and return the largest found
    for (int i = 0; i < (length - 4); i++){
        
        product = digit_map[n[i]] * digit_map[n[i+1]] * digit_map[n[i+2]] * digit_map[n[i+3]] * digit_map[n[i+4]];

        if (product > largest_product){
            largest_product = product;
        }
    }

    return largest_product;
 }