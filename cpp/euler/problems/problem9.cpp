#include "problem9.h"
#include <cmath>

using namespace std;

Problem9::Problem9() 
 : Problem(9, "Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.")
 {
 }


 int64_t Problem9::solve()
 {
    int product = 0;
    float c = 0;
    int a = 1;
    int b = 1;
    bool found = false;

    for (a = 1; a < 500; a++)
    {
        for (b = 1; b < 500; b++)
        {
            c = sqrt((float)((a*a) + (b*b)));
            if (a + b + c == 1000)
            {
                found = true;
                break;
            }
        }

        if (found) break;
    }

    product = a * b * (int)c;

    return product;
 }

 int64_t Problem9::chat_gpt_solve()
 {
    /*
        Prompt:

            Use C++ to solve this problem. Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.
    */

    /*
        The code uses two nested loops to generate all possible pairs (a, b) where a < b, and then calculates c = 1000 - a - b. 
        It checks whether a^2 + b^2 = c^2, and if so, outputs the values of a, b, and c along with their product abc.
    */
    
    for (int a = 1; a <= 1000; a++) {
        for (int b = a + 1; b <= 1000; b++) {
            int c = 1000 - a - b;
            if (a*a + b*b == c*c) {
                return a * b * c;
            }
        }
    }
    return 0;
 }