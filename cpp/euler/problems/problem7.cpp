#include "problem7.h"
#include "libraries/primes.h"

using namespace euler::primes;

Problem7::Problem7() 
 : Problem(7, "Find the 10001st prime.")
 {
 }


 int64_t Problem7::solve()
 {
    return nth_prime(10001);
 }