#include "problem.h"
#include "libraries/stopwatch.h"

#include <iostream>

using namespace std;
using namespace euler::timing;

// Solve and print out result

Problem::Problem(int number, const string &description) :
    m_number(number), m_description(description){
}

int Problem::number(){
    return m_number;
}

string Problem::description(){
    return m_description;
}

int Problem::answer(){
    return m_answer;
}

double Problem::elapsed_milliseconds(){
    return m_elapsed_milliseconds;
}

// int64_t Problem::chat_gpt_solve(){
//     return 0;
// }

void Problem::run(){
    {
        Stopwatch stopwatch;
        stopwatch.start();
        m_answer = solve();
        stopwatch.stop();
        m_elapsed_milliseconds = stopwatch.elapsed_milliseconds();
    }
    {
        Stopwatch stopwatch;
        stopwatch.start();
        m_chat_gpt_answer = chat_gpt_solve();
        stopwatch.stop();
        m_chat_gpt_elapsed_milliseconds = stopwatch.elapsed_milliseconds();
    }
}

void Problem::report(bool redact_answer){
    cout << to_string(m_number) << ". " << m_description << endl;
    string answer = to_string(m_answer);
    string chat_gpt_answer = to_string(m_chat_gpt_answer);
    if(redact_answer){
        answer = "REDACTED";
        chat_gpt_answer = "REDACTED";
    }
    cout << "Answer: " << answer << " in " << m_elapsed_milliseconds << "ms" << endl;
    cout << "Chat GPT answer: " << chat_gpt_answer << " in " << m_chat_gpt_elapsed_milliseconds << "ms" << endl;
}