#ifndef TEN_PROBLEMS_PROBLEM3_H
#define TEN_PROBLEMS_PROBLEM3_H


#include "problem.h"

class Problem3 : public Problem
{
public:
    
    Problem3();

    int64_t solve();

};


#endif