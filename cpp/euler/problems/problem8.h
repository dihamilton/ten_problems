#ifndef TEN_PROBLEMS_PROBLEM8_H
#define TEN_PROBLEMS_PROBLEM8_H


#include "problem.h"

class Problem8 : public Problem
{
public:
    
    Problem8();

    int64_t solve();

};


#endif