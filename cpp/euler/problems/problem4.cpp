#include "problem4.h"
#include "libraries/general_maths.h"

using namespace euler::maths;

Problem4::Problem4() 
 : Problem(4, "Find the largest palindrome made from the product of two 3-digit numbers.")
 {
 }


 int64_t Problem4::solve()
 {
    int low = 100;
    int high = 999;
    int num;
    int largest_palindrome = 0;

    for (int i = high; i >= low; i--)
    {
        for (int j = 990; j >= low; j -= 11)
        {
            num = i * j;
            if (is_palindrome(num))
            {
                if (num > largest_palindrome)
                    largest_palindrome = num;
            }
        }
    }
    return largest_palindrome;
 }