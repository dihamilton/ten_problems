#include "problem10.h"

#include "libraries/primes.h"

#include <vector>
#include <iostream>

using namespace std;
using namespace euler::primes;

Problem10::Problem10() 
 : Problem(10, "Calculate the sum of all the primes below one million.")
 {
 }

 int64_t Problem10::solve()
 {
    // Get all the primes below one million
    vector<int> primes = sieve(1000000);
    
     // Sum them up
    int64_t sum = 0;
    for(auto prime : primes){
        sum += prime;
    }

    return sum;
 }


 int64_t Problem10::chat_gpt_solve()
 {
    /*
        Prompt:

            Use C++ to solve this problem. Calculate the sum of all the primes below one million.
    */

    /*
        In this program, we first create a boolean vector is_prime of size N and initialize all elements to true.
        We then use the Sieve of Eratosthenes algorithm to mark all non-prime numbers as false.

        After that, we iterate through the is_prime vector and add up all the primes to obtain the sum. 
        Finally, we print the result to the console.

        Note that we use a long long variable for the sum to avoid overflow.
    */
    
    const int N = 1000000;
    vector<bool> is_prime(N, true);
    is_prime[0] = is_prime[1] = false;

    // Sieve of Eratosthenes algorithm
    for (int i = 2; i * i <= N; i++) {
        if (is_prime[i]) {
            for (int j = i * i; j < N; j += i) {
                is_prime[j] = false;
            }
        }
    }

    // Calculate the sum of primes
    long long sum = 0;
    for (int i = 2; i < N; i++) {
        if (is_prime[i]) {
            sum += i;
        }
    }

    return sum;
 }