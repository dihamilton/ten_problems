#ifndef TEN_PROBLEMS_PROBLEM7_H
#define TEN_PROBLEMS_PROBLEM7_H


#include "problem.h"

class Problem7 : public Problem
{
public:
    
    Problem7();

    int64_t solve();

};


#endif