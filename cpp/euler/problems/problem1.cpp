#include "problem1.h"

Problem1::Problem1() 
 : Problem(1, "Add all the natural numbers below 1000 that are multiples of 3 or 5.")
 {
 }


int64_t Problem1::solve()
{
    int limit = 1000;
    int sum = 0; // sum of all the multiples

    for (int i = 0; i < limit; i++){
        if (i % 3 == 0 || i % 5 == 0){
            sum += i;
        }
    }

    return sum;
}

int64_t Problem1::chat_gpt_solve()
{
    /*
        Prompt:

            Use C++ to solve this problem. 
            If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
            Find the sum of all the multiples of 3 or 5 below 1000.
    */

    /*
        Explanation:

            - We initialize a variable sum to zero to accumulate the sum of multiples of 3 or 5.
            - We use a for loop to iterate over all natural numbers from 1 to 999 (i.e., below 1000).
            - Inside the loop, we use the modulo operator % to check if the current number i is divisible by 3 or 5.
            - If i is divisible by 3 or 5, we add it to sum.
    */

    int sum = 0;
    for (int i = 1; i < 1000; i++) {
        if (i % 3 == 0 || i % 5 == 0) {
            sum += i;
        }
    }

    return sum;
}