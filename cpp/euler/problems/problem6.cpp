#include "problem6.h"

Problem6::Problem6() 
 : Problem(6, "What is the difference between the sum of the squares and the square of the sums?")
 {
 }


 int64_t Problem6::solve()
 {
    int num = 100;

    int sum_of_squares = 0;
    int square_of_sum = 0;

    for (int i = 1; i <= num; i++)
    {
        sum_of_squares += i*i;
        square_of_sum += i;
    }

    square_of_sum *= square_of_sum;

    int difference = square_of_sum - sum_of_squares;

    return difference;
 }