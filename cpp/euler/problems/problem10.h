#ifndef TEN_PROBLEMS_PROBLEM10_H
#define TEN_PROBLEMS_PROBLEM10_H


#include "problem.h"

class Problem10 : public Problem
{
public:
    
    Problem10();

    int64_t solve();

     int64_t chat_gpt_solve();

};


#endif