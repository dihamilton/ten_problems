#include "problem2.h"

Problem2::Problem2() 
 : Problem(2, "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million.")
 {
 }

 int64_t Problem2::solve()
 {
    int a = 1;
    int b = 2;
    int c = 0;
    int sum = 2;  // Already seen one even number

    while (c < 1000000)
    {
        c = a + b;
        if (c % 2 == 0)
        {
            sum += c;
        }
        a = b;
        b = c;
    }

    return sum;
 }