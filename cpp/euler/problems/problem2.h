#ifndef TEN_PROBLEMS_PROBLEM2_H
#define TEN_PROBLEMS_PROBLEM2_H

#include "problem.h"

class Problem2 : public Problem
{
public:
    Problem2();
    int64_t solve();
};

#endif