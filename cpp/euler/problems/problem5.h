#ifndef TEN_PROBLEMS_PROBLEM5_H
#define TEN_PROBLEMS_PROBLEM5_H


#include "problem.h"

class Problem5 : public Problem
{
public:
    
    Problem5();

    int64_t solve();

};


#endif