#ifndef TEN_PROBLEMS_PROBLEM6_H
#define TEN_PROBLEMS_PROBLEM6_H


#include "problem.h"

class Problem6 : public Problem
{
public:
    
    Problem6();

    int64_t solve();

};


#endif