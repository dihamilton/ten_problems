#ifndef TEN_PROBLEMS_PROBLEM9_H
#define TEN_PROBLEMS_PROBLEM9_H


#include "problem.h"

class Problem9 : public Problem
{
public:
    
    Problem9();

    int64_t solve();

    int64_t chat_gpt_solve();

};


#endif