#include <iostream>
#include <vector>
#include <memory>
#include <thread>


#include "problems/problem1.h"
#include "problems/problem2.h"
#include "problems/problem3.h"
#include "problems/problem4.h"
#include "problems/problem5.h"
#include "problems/problem6.h"
#include "problems/problem7.h"
#include "problems/problem8.h"
#include "problems/problem9.h"
#include "problems/problem10.h"
#include "libraries/stopwatch.h"

using namespace std;
using namespace euler::timing;


double run_threaded(const vector<shared_ptr<Problem>> &problems);

int main()
{
    bool redact_answers = false;
    bool use_threads = false;
    
    vector<shared_ptr<Problem>> problems = {
        make_shared<Problem1>(),
        make_shared<Problem2>(),
        make_shared<Problem3>(),
        make_shared<Problem4>(),
        make_shared<Problem5>(),
        make_shared<Problem6>(),
        make_shared<Problem7>(),
        make_shared<Problem8>(),
        make_shared<Problem9>(),
        make_shared<Problem10>()
    };
    
    double total_elapsed_milliseconds = 0.0;
    double total_real_elapsed_milliseconds = 0.0;

    if(use_threads){
        total_real_elapsed_milliseconds = run_threaded(problems);
    }else{
        for(const auto &problem : problems){
            problem->run();
        }
    }

    for(const auto &problem : problems){
        problem->report(redact_answers);
        total_elapsed_milliseconds += problem->elapsed_milliseconds();
    }

    cout << "C++ total elapsed: " << total_elapsed_milliseconds << "ms" << endl;
    if(use_threads){
        cout << "C++ total real time elapsed: " << total_real_elapsed_milliseconds << "ms" << endl;
    }
    
    return 0;
}

double run_threaded(const vector<shared_ptr<Problem>> &problems)
{
    vector<thread> threads;

    Stopwatch stopwatch;
    stopwatch.start();

    for(const auto &problem : problems){
        thread t([&] { problem->run(); });
        threads.push_back(move(t));
    }

    for(auto &t : threads){
        t.join();
    }

    stopwatch.stop();
    return stopwatch.elapsed_milliseconds();
}
