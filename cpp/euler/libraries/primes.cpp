
#include <iostream>
#include <vector>
#include <map>
#include <math.h>
#include <cmath>
#include "primes.h"
#include "stopwatch.h"

using namespace std;

namespace euler{
    namespace primes{
 

// Sieve of Eratosthenes
// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
vector<int> sieve(int n){

    vector<bool> integers(n+1);
    vector<int> primes;
    // Create a list of odd integers from 3 up, because we already know 2 is the only even prime
    for(int i = 3; i < n; i += 2){
        integers[i] = true; // All these integers are possibly prime
    }
    // Since we're not including it above, let's add 2 to our list of known primes
    primes.push_back(2);

    int p = 3;
    int multipleOfP = 0;
    while(p*p <= n){    // We can stop looking when p² is greater than n

        multipleOfP = p*p;  // Start from p² since smaller multiples of p will have already been marked
                            // e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                            // and even numbers have already been discounted

        // Strike out the multiples of p all the way up to n
        while(multipleOfP <= n){
            integers[multipleOfP] = false;  // Since this number has at least one factor - p, it's not prime
            multipleOfP += 2*p;     // Go up in steps of 2p to avoid even numbers which are already out of the list
                                    // e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        }

        // Find the next number that isn't struck off 
        while(p <= n){
            p += 2;
            if(integers[p] == true){
                break;
            }
        }

    }
    
    for(int i = 0; i < n; i++){
        if(integers[i]){
            primes.push_back(i);
        }
    }

    return primes;
}

map<int, int> prime_factorise(int64_t n, int max_expected_factor)
{
    vector<int> primes = sieve(max_expected_factor);

    map<int, int> factors;  // Prime factors and how many times they occur
    int64_t factor = 0;

    int64_t limit = n/2;

    for(auto prime : primes){

        if(prime > limit) break;
        factor = prime;

        // If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        // our prime divide the candidate so we can add it again.
        // E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        // and so on.
        while(n % factor == 0)
        {
            factors[prime]++;
            factor *= prime;
        }

    }

    // If there are no factors then n is prime, which means it is divided only by itself once
    if(factors.size() == 0){
        factors[n] = 1;
    }

    return factors;
}



int nth_prime(int n){

    // Value of the nth prime is approximately n*ln(n)
    int m = 2 * n;
    int approx_value = m * log(m);
    // Generate sieve up to this value
    vector<int> primes = sieve(approx_value);
    return primes[n-1];
}


    }
}