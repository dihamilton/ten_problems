#ifndef TEN_PROBLEMS_GENERAL_MATHS_H
#define TEN_PROBLEMS_GENERAL_MATHS_H

#include <vector>
#include <map>

namespace euler{
    namespace maths{
 

std::vector<int> to_digits(int n);
bool is_palindrome(std::vector<int> n);
bool is_palindrome(int n);

    }
}

#endif