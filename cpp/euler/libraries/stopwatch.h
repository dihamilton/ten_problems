#ifndef TEN_PROBLEMS_STOPWATCH_H
#define TEN_PROBLEMS_STOPWATCH_H

#include <chrono>
#include <ctime>

namespace euler{
	namespace timing{



class Stopwatch
{
public:
	Stopwatch();

	void start();
	void stop();
    double elapsed_seconds();
	double running_elapsed_seconds();
	double elapsed_milliseconds();
	double running_elapsed_milliseconds();

private:

	std::chrono::high_resolution_clock::time_point m_start;
    std::chrono::high_resolution_clock::time_point m_stop;

};

	}
}

#endif