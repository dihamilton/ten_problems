#ifndef TEN_PROBLEMS_PRIMES_H
#define TEN_PROBLEMS_PRIMES_H

#include <vector>
#include <map>

namespace euler{
    namespace primes{

std::vector<int> sieve(int n);
std::map<int, int> prime_factorise(int64_t n, int max_expected_factor);
int nth_prime(int n);
        
    }
}

#endif