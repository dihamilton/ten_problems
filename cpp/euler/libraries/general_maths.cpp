#include "general_maths.h"


using namespace std;

namespace euler{
    namespace maths{
 

vector<int> to_digits(int n)
{
    vector<int> digits;
    int temp = n;
    int rem = 0;
    while (temp > 0)
    {
        rem = temp % 10;
        digits.push_back(rem);
        temp /= 10;
    }

    return digits;
}

bool is_palindrome(vector<int> digits)
{
    int size = static_cast<int>(digits.size());
    int halfway = size / 2;
    for (int i = 0; i < halfway; i++)
    {
        if (digits[i] != digits[(size - 1) - i])
            return false;
    }

    return true;
}

bool is_palindrome(int n)
{
    return is_palindrome(to_digits(n));
}

    }      
}