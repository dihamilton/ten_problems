
#include "stopwatch.h"

using namespace std;

namespace euler{
	namespace timing{


Stopwatch::Stopwatch()
{
}

void Stopwatch::start()
{
	m_start = chrono::high_resolution_clock::now();
}

void Stopwatch::stop()
{
	m_stop = chrono::high_resolution_clock::now();
}


double Stopwatch::elapsed_seconds()
{
    chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(m_stop - m_start);
	return time_span.count();
}

double Stopwatch::running_elapsed_seconds()
{
    chrono::duration<double> time_span = chrono::duration_cast<chrono::duration<double>>(chrono::high_resolution_clock::now() - m_start);
	return time_span.count();
}

double Stopwatch::elapsed_milliseconds()
{
	return elapsed_seconds() * 1000;
}

double Stopwatch::running_elapsed_milliseconds()
{
	return running_elapsed_seconds() * 1000;
}

		
	}
}