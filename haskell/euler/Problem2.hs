module Problem2 where

number = 2::Integer
description = "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million."::String
solution = (sum [x | x <- [1..999], x `mod` 3 == 0 || x `mod` 5 == 0])::Integer
