module Problem1 where

number = 1::Integer
description = "Add all the natural numbers below 1000 that are multiples of 3 or 5."::String
solution = (sum [x | x <- [1..999], x `mod` 3 == 0 || x `mod` 5 == 0])::Integer
