import Text.Printf
import Problem1
import Problem2

report :: (Integer, String, Integer) -> IO()
report (number, description, solution) = printf "%d. %s\n%d\n" number description solution

main = do
    report (Problem1.number, Problem1.description, Problem1.solution)
    report (Problem2.number, Problem2.description, Problem2.solution)