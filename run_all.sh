#!/bin/bash

languages=( c cpp csharp go javascript kotlin php python ruby rust )
echo "["
for i in {1..10}
do
   echo "{"
   for language in "${languages[@]}"
   do
      docker-compose run $language | docker-compose run check
   done
   echo "},"
done
echo "]"