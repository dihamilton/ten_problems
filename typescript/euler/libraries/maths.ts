function ToDigits(n: number): Array<number> {
  let digits = new Array<number>();
  let temp = n;
  let remainder = 0;
  while (temp > 0) {
    remainder = Math.round(temp % 10);
    digits.push(remainder);
    temp = Math.floor(temp / 10);
  }
  return digits;
}

function IsPalindromeArray(digits: Array<number>): boolean {
  let size = digits.length;
  let halfway = size / 2;
  for (let i = 0; i < halfway; i++) {
    if (digits[i] != digits[size - 1 - i]) return false;
  }

  return true;
}

export function IsPalindrome(n: number): boolean {
  return IsPalindromeArray(ToDigits(n));
}
