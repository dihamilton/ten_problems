import { Problem } from "./problem";
import * as primes from "../libraries/primes";

export class Problem10 extends Problem {
  constructor() {
    super(10, "Calculate the sum of all the primes below one million.");
  }

  solve(): number {
    // Get all the primes below one million
    let p = primes.Sieve(1000000);

    // Sum them up
    let sum = 0;
    for (let i = 0; i < p.length; i++) {
      sum += p[i];
    }

    return sum;
  }
}
