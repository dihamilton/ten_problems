import { Problem } from "./problem";

export class Problem2 extends Problem {
  constructor() {
    super(
      2,
      "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million."
    );
  }

  solve(): number {
    let a = 1;
    let b = 2;
    let c = 0;
    let sum = 2; // Already seen one even number

    while (c < 1000000) {
      c = a + b;
      if (c % 2 == 0) {
        sum += c;
      }
      a = b;
      b = c;
    }

    return sum;
  }
}
