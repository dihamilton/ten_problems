import * as perf from "perf_hooks";

export abstract class Problem {
  problemNumber: number;
  description: string;
  answer: number = 0;
  elapsedMilliseconds: number = 0;

  constructor(problemNumber: number, description: string) {
    this.problemNumber = problemNumber;
    this.description = description;
  }

  run() {
    let start = perf.performance.now();
    this.answer = this.solve();
    let stop = perf.performance.now();
    this.elapsedMilliseconds = stop - start; // performance.now is in milliseconds
  }

  report() {
    console.log(`${this.problemNumber}. ${this.description}`);
    console.log(`Answer: ${this.answer} in ${this.elapsedMilliseconds}ms.`);
  }

  // Child classes should override solve
  abstract solve(): number;
}
