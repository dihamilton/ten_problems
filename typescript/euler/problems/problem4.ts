import { Problem } from "./problem";
import * as maths from "../libraries/maths";

export class Problem4 extends Problem {
  constructor() {
    super(
      4,
      "Find the largest palindrome made from the product of two 3-digit numbers."
    );
  }

  solve(): number {
    let low = 100;
    let high = 999;
    let num;
    let largestPalindrome = 0;

    for (let i = high; i >= low; i--) {
      for (let j = 990; j >= low; j -= 11) {
        num = i * j;
        if (maths.IsPalindrome(num)) {
          if (num > largestPalindrome) {
            largestPalindrome = num;
          }
        }
      }
    }
    return largestPalindrome;
  }
}
