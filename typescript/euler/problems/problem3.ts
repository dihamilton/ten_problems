import { Problem } from "./problem";
import * as primes from "../libraries/primes";

export class Problem3 extends Problem {
  constructor() {
    super(3, "Find the largest prime factor of a composite number.");
  }

  solve(): number {
    let num = 600851475143;
    let factors = primes.PrimeFactorise(num, 10000);
    // Since we identify factors from smallest to largest we can use the end of the list
    return Array.from(factors)[factors.size - 1][0];
  }
}
