import { Problem } from "./problem";

export class Problem6 extends Problem {
  constructor() {
    super(
      6,
      "What is the difference between the sum of the squares and the square of the sums?"
    );
  }

  solve(): number {
    let difference = 0;
    let num = 100;

    let sumOfSquares = 0;
    let squareOfSum = 0;

    for (let i = 1; i <= num; i++) {
      sumOfSquares += i * i;
      squareOfSum += i;
    }

    squareOfSum *= squareOfSum;

    difference = squareOfSum - sumOfSquares;

    return difference;
  }
}
