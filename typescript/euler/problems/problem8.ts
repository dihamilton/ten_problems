import { Problem } from "./problem";
import * as fs from "fs";

export class Problem8 extends Problem {
  constructor() {
    super(
      8,
      "Find the greatest product of five consecutive digits in the 1000-digit number."
    );
  }

  solve(): number {
    let contents = fs.readFileSync("/data/8.txt");
    const n = contents.toString();

    // Here we could use the property that the numeric value of a digit char is the ascii value minus 48 but this seems more obvious
    const digitMap: any = {
      "0": 0,
      "1": 1,
      "2": 2,
      "3": 3,
      "4": 4,
      "5": 5,
      "6": 6,
      "7": 7,
      "8": 8,
      "9": 9,
    };

    let largestProduct = 0;
    let product = 0;

    let length = n.length;
    // Go through and calculate all the 5 consecutive digit products in the number and return the largest found
    for (let i = 0; i < length - 4; i++) {
      product =
        digitMap[n[i]] *
        digitMap[n[i + 1]] *
        digitMap[n[i + 2]] *
        digitMap[n[i + 3]] *
        digitMap[n[i + 4]];

      if (product > largestProduct) {
        largestProduct = product;
      }
    }

    return largestProduct;
  }
}
