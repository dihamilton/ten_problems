import { Problem } from "./problem";

export class Problem9 extends Problem {
  constructor() {
    super(
      9,
      "Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000."
    );
  }

  solve(): number {
    let product = 0;
    let c = 0;
    let a = 1;
    let b = 1;
    let found = false;

    for (a = 1; a < 500; a++) {
      for (b = 1; b < 500; b++) {
        c = Math.sqrt(a * a + b * b);
        if (a + b + c == 1000) {
          found = true;
          break;
        }
      }

      if (found) break;
    }

    product = a * b * c;

    return product;
  }
}
