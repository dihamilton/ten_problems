import { Problem } from "./problem";

export class Problem1 extends Problem {
  constructor() {
    super(
      1,
      "Add all the natural numbers below 1000 that are multiples of 3 or 5."
    );
  }

  solve(): number {
    const limit = 1000;
    let sum = 0; // sum of all the multiples

    for (let i = 0; i < limit; i++) {
      if (i % 3 == 0 || i % 5 == 0) {
        sum += i;
      }
    }
    return sum;
  }
}
