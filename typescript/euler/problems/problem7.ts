import { Problem } from "./problem";
import * as primes from "../libraries/primes";

export class Problem7 extends Problem {
  constructor() {
    super(7, "Find the 10001st prime.");
  }

  solve(): number {
    return primes.NthPrime(10001);
  }
}
