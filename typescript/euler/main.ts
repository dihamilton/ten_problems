import { Problem } from "./problems/problem";
import { Problem1 } from "./problems/problem1";
import { Problem2 } from "./problems/problem2";
import { Problem3 } from "./problems/problem3";
import { Problem4 } from "./problems/problem4";
import { Problem5 } from "./problems/problem5";
import { Problem6 } from "./problems/problem6";
import { Problem7 } from "./problems/problem7";
import { Problem8 } from "./problems/problem8";
import { Problem9 } from "./problems/problem9";
import { Problem10 } from "./problems/problem10";

function main() {
  let allProblems: Array<Problem> = [
    new Problem1(),
    new Problem2(),
    new Problem3(),
    new Problem4(),
    new Problem5(),
    new Problem6(),
    new Problem7(),
    new Problem8(),
    new Problem9(),
    new Problem10(),
  ];

  let totalElapsedMilliseconds = 0.0;
  allProblems.forEach((p) => {
    p.run();
    p.report();
    totalElapsedMilliseconds += p.elapsedMilliseconds;
  });

  console.log("TS/JS total elapsed: " + totalElapsedMilliseconds + "ms");
}

main();
