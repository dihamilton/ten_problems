// Strong types!
var greeting: string = "Hello";
var recipient: string = "World";

console.log('TypeScript/JavaScript: ' + greeting + " " + recipient + "!"); 