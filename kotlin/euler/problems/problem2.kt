class Problem2 : Problem {

    constructor() : super(2, "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million.") {
    }
    

    override fun solve(): Long{
        var a = 1;
        var b = 2;
        var c = 0;
        var sum: Long = 2;  // Already seen one even number

        while (c < 1000000)
        {
            c = a + b;
            if (c % 2 == 0)
            {
                sum += c;
            }
            a = b;
            b = c;
        }

        return sum;
    }
}