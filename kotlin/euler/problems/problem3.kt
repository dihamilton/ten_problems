
class Problem3 : Problem {

    constructor() : super(3, "Find the largest prime factor of a composite number.") {
    }
    

    override fun solve(): Long{
        var num = 600851475143
        var factors = primeFactorise(num, 10000);
        // Since we identify factors from smallest to largest we can use the end of the list
        return factors.toList().last().first;
    }
}