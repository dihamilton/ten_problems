
class Problem7 : Problem {

    constructor() : super(7, "Find the 10001st prime.") {
    }
    

    override fun solve(): Long{
        return nthPrime(10001).toLong()
    }
}