
class Problem6 : Problem {

    constructor() : super(6, "What is the difference between the sum of the squares and the square of the sums?") {
    }
    

    override fun solve(): Long{
        var difference: Int
        var num = 100

        var sumOfSquares = 0
        var squareOfSum = 0

        for (i in 1..num)
        {
            sumOfSquares += i*i
            squareOfSum += i
        }

        squareOfSum *= squareOfSum

        difference = squareOfSum - sumOfSquares

        return difference.toLong()
    }
}