
class Problem1 : Problem {

    constructor() : super(1, "Add all the natural numbers below 1000 that are multiples of 3 or 5.") {
    }
    

    override fun solve(): Long{
        val limit = 1000
        var sum: Long = 0 // sum of all the multiples

        for (i in 0..(limit-1))
        {
            if (i % 3 == 0 || i % 5 == 0)
            {
                sum += i
            }
        }

        return sum
    }
}