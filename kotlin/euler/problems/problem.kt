import kotlin.system.*

abstract class Problem
{
    var number: Int = 0
    var description: String
    var answer: Long = 0
    var elapsedMilliseconds: Double = 0.0
    
    constructor(number: Int, description: String){
        this.number = number
        this.description = description
    }

    fun elapsedMilliseconds() : Double{
        return this.elapsedMilliseconds;
    }
    
    fun run(){
        var elapsedNanoseconds = measureNanoTime{
            this.answer = solve();
        }
        this.elapsedMilliseconds = elapsedNanoseconds / 1000000.0
    }

    fun report(){

        println(this.number.toString() + ": " + this.description)
        println("Answer: " + this.answer.toString() + " in " + this.elapsedMilliseconds.toString() + "ms")
    }

    // Child classes should override
    abstract fun solve() : Long

};