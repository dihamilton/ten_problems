
class Problem4 : Problem {

    constructor() : super(4, "Find the largest palindrome made from the product of two 3-digit numbers.") {
    }
    

    override fun solve(): Long{
        var low = 100
        var high = 999
        var num: Int
        var largestPalindrome = 0

        for(i in 990 downTo low step 11) // One of the numbers must be divisible by 11
        {
            for (j in high downTo low)
            {
                num = i * j
                if (isPalindrome(num))
                {
                    if (num > largestPalindrome){
                        largestPalindrome = num
                    }
                    break   // As soon as we find a palindrome in the inner loop we can break - it will be the highest possible
                            // palindrome resulting from i * j for the value of i we are looking at.
                }
            }
        }
        return largestPalindrome.toLong()
    }
}