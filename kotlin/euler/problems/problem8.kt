import java.io.File

class Problem8 : Problem {

    constructor() : super(8, "Find the greatest product of five consecutive digits in the 1000-digit number.") {
    }
    

    override fun solve(): Long{
        val n = File("/data/8.txt").readText()

        // Here we could use the property that the numeric value of a digit char is the ascii value minus 48 but this seems more obvious
        val digitMap = mapOf('0' to 0, '1' to 1, '2' to 2, '3' to 3, '4' to 4, '5' to 5, '6' to 6, '7' to 7, '8' to 8, '9' to 9)
        var largestProduct = 0
        var product : Int

        val length = n.length
        // Go through and calculate all the 5 consecutive digit products in the number and return the largest found
        for (i in 0..(length - 5)){
            // !! is a shorthand for converting to the non nullable version and throw an exception if it's not possible
            // e.g. Int? to Int. In a production system it's better to handle potential nulls explicitly but for this
            // use case we know the input well
            product = digitMap[n[i]]!! * digitMap[n[i+1]]!! * digitMap[n[i+2]]!! * digitMap[n[i+3]]!! * digitMap[n[i+4]]!!;

            if (product > largestProduct){
                largestProduct = product
            }
        }

        return largestProduct.toLong()
    }
}