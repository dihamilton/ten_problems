
class Problem10 : Problem {

    constructor() : super(10, "Calculate the sum of all the primes below one million.") {
    }
    

    override fun solve(): Long{
        // Get all the primes below one million
        val primes = sieve(1000000)
        
        // Sum them up
        var sum = 0L
        for(prime in primes){
            sum += prime
        }

        return sum
    }
}