import kotlin.math.*

class Problem9 : Problem {

    constructor() : super(9, "Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.") {
    }
    

    override fun solve(): Long{
        var product: Int
        var c = 0.0
        var finalA = 0
        var finalB = 1
        var found = false

        for (a in 1..500)
        {
            for (b in 1..500)
            {
                c = sqrt(((a*a) + (b*b)).toDouble())
                // Do this comparison as a double since we want it to match exactly
                if (a.toDouble() + b.toDouble() + c == 1000.0)
                {
                    found = true
                    finalA = a
                    finalB = b
                    break
                }
            }

            if (found) break
        }

        product = finalA * finalB * c.toInt()

        return product.toLong()
    }
}