
fun toDigits(n: Int) : List<Int>
{
    var digits = mutableListOf<Int>()
    var temp = n
    var remainder: Int
    while (temp > 0)
    {
        remainder = temp % 10
        digits.add(remainder)
        temp /= 10
    }

    return digits;
}

fun isPalindrome(digits: List<Int>) : Boolean
{
    var size = digits.count()
    var halfway = size / 2
    for(i in 0 until halfway)
    {
        if (digits[i] != digits[(size - 1) - i])
            return false
    }

    return true
}

fun isPalindrome(n: Int) : Boolean
{
    return isPalindrome(toDigits(n))
}
