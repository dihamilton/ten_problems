import kotlin.math.*

// Sieve of Eratosthenes
// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
fun sieve(n: Int): List<Int>{

    var integers = BooleanArray(n+1)
    var primes = mutableListOf<Int>()
    // Create a list of odd integers from 3 up, because we already know 2 is the only even prime
    for(i in 3 until n step 2){
        integers[i] = true // All these integers are possibly prime
    }
    // Since we're not including it above, let's add 2 to our list of known primes
    primes.add(2)

    var p = 3
    var multipleOfP: Int
    while(p*p <= n){    // We can stop looking when p² is greater than n

        multipleOfP = p*p   // Start from p² since smaller multiples of p will have already been marked
                            // e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                            // and even numbers have already been discounted

        // Strike out the multiples of p all the way up to n
        while(multipleOfP <= n){
            integers[multipleOfP] = false  // Since this number has at least one factor - p, it's not prime
            multipleOfP += 2*p      // Go up in steps of 2p to avoid even numbers which are already out of the list
                                    // e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        }

        // Find the next number that isn't struck off 
        while(p <= n){
            p += 2
            if(integers[p] == true){
                break
            }
        }
    }
    
    for(i in 0 until n){
        if(integers[i]){
            primes.add(i)
        }
    }

    return primes
}

fun primeFactorise(n: Long, maxExpectedFactor: Int) : Map<Long, Long>
{
    var primes = sieve(maxExpectedFactor)

    var factors = mutableMapOf<Long, Long>()
    
    var factor: Long
    var limit: Long = n/2

    for(prime in primes)
    {
        var p = prime.toLong()
        if(p > limit) break
        factor = p

        // If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        // our prime divide the candidate so we can add it again.
        // E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        // and so on
        while(n % factor == 0L)
        {
            if(!factors.containsKey(p)){
                factors[p] = 0
            }
            factors[p] = factors[p]!!.plus(1)
            factor *= p
        }
    }

    // If there are no factors then n is prime, which means it is divided only by itself once
    if(factors.count() == 0){
       factors[n] = 1
    }

    return factors
}

fun nthPrime(n: Int) : Int
{
    // Value of the nth prime is approximately n*ln(n)
    var m : Double = 1.5 * n.toDouble()   // Fudge it high just in case
    var approxValue = (m * ln(m)).toInt()
    // Generate sieve up to this value
    var primes = sieve(approxValue);
    return primes[n-1]
}

