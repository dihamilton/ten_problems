




fun main() {
    val problems = listOf(Problem1(), Problem2(), Problem3(), Problem4(), Problem5(), Problem6(), Problem7(), Problem8(), Problem9(), Problem10())
    var totalElapsedMilliseconds = 0.0
    for(problem in problems){
        problem.run()
        problem.report()
        totalElapsedMilliseconds += problem.elapsedMilliseconds()
    }

    println("Kotlin total elapsed: " + totalElapsedMilliseconds.toString() + "ms")
}