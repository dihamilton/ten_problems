using System.Collections.Generic;

namespace Euler
{
class Maths
{
public static List<int> ToDigits(int n)
{
    List<int> digits = new List<int>();
    int temp = n;
    int remainder = 0;
    while (temp > 0)
    {
        remainder = temp % 10;
        digits.Add(remainder);
        temp /= 10;
    }

    return digits;
}

public static bool IsPalindrome(List<int> digits)
{
    int size = digits.Count;
    int halfway = size / 2;
    for (int i = 0; i < halfway; i++)
    {
        if (digits[i] != digits[(size - 1) - i])
            return false;
    }

    return true;
}

public static bool IsPalindrome(int n)
{
    return IsPalindrome(ToDigits(n));
}

}

}
