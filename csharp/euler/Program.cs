﻿using System;
using System.Collections.Generic;

namespace Euler
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Problem> problems = new List<Problem>(){ 
                new Problem1(),
                new Problem2(),
                new Problem3(),
                new Problem4(),
                new Problem5(),
                new Problem6(),
                new Problem7(),
                new Problem8(),
                new Problem9(),
                new Problem10()
            };

            double totalElapsedMilliseconds = 0;
            foreach(var problem in problems){
                var p = new Problem1();
                problem.Run();
                problem.Report();
                totalElapsedMilliseconds += problem.ElapsedMilliseconds;
            }
            
            Console.WriteLine("C# total elapsed: " + totalElapsedMilliseconds.ToString() + "ms");
        }
    }
}
