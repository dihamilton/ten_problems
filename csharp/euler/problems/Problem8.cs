using System;
using System.IO;
using System.Collections.Generic;

namespace Euler
{
    public class Problem8 : Problem{

        public Problem8() : base(8, "Find the greatest product of five consecutive digits in the 1000-digit number.")
        {}

        protected override Int64 Solve(){
            string n = File.ReadAllText("/data/8.txt");

            // Here we could use the property that the numeric value of a digit char is the ascii value minus 48 but this seems more obvious
            Dictionary<char, int> digitMap = new Dictionary<char, int>(){ {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4}, {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9} };
            
            int largestProduct = 0;
            int product = 0;

            int length = n.Length;
            // Go through and calculate all the 5 consecutive digit products in the number and return the largest found
            for (int i = 0; i < (length - 4); i++){
                
                product = digitMap[n[i]] * digitMap[n[i+1]] * digitMap[n[i+2]] * digitMap[n[i+3]] * digitMap[n[i+4]];

                if (product > largestProduct){
                    largestProduct = product;
                }
            }

            return largestProduct;
        }
    }
}