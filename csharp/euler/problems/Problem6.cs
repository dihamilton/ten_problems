using System;

namespace Euler
{
    public class Problem6 : Problem{

        public Problem6() : base(6, "What is the difference between the sum of the squares and the square of the sums?")
        {}

        protected override Int64 Solve(){
            int difference = 0;
            int num = 100;

            int sumOfSquares = 0;
            int squareOfSum = 0;

            for (int i = 1; i <= num; i++)
            {
                sumOfSquares += i*i;
                squareOfSum += i;
            }

            squareOfSum *= squareOfSum;

            difference = squareOfSum - sumOfSquares;

            return difference;
        }
    }
}