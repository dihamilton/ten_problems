using System;
using System.IO;

namespace Euler
{
    public class Problem10 : Problem{

        public Problem10() : base(10, "Calculate the sum of all the primes below one million.")
        {}

        protected override Int64 Solve(){
            // Get all the primes below one million
            var primes = Primes.Sieve(1000000);
            
            // Sum them up
            Int64 sum = 0;
            foreach(var prime in primes){
                sum += prime;
            }

            return sum;
        }
    }
}