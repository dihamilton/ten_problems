using System;

namespace Euler
{
    public class Problem4 : Problem{

        public Problem4() : base(4, "Find the largest palindrome made from the product of two 3-digit numbers.")
        {}

        protected override Int64 Solve(){
            int low = 100;
            int high = 999;
            int num;
            int largestPalindrome = 0;

            for (int i = high; i >= low; i--)
            {
                for (int j = 990; j >= low; j -= 11)
                {
                    num = i * j;
                    if (Maths.IsPalindrome(num))
                    {
                        if (num > largestPalindrome)
                            largestPalindrome = num;
                    }
                }
            }
            return largestPalindrome;
        }
    }
}