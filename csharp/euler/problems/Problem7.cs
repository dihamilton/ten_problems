using System;

namespace Euler
{
    public class Problem7 : Problem{

        public Problem7() : base(7, "Find the 10001st prime.")
        {}

        protected override Int64 Solve(){
            return Primes.NthPrime(10001);
        }
    }
}