using System;

namespace Euler
{
    public class Problem1 : Problem{

        public Problem1() : base(1, "Add all the natural numbers below 1000 that are multiples of 3 or 5.")
        {}

        protected override Int64 Solve(){
            int limit = 1000;
            int sum = 0; // sum of all the multiples

            for (int i = 0; i < limit; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum += i;
                }
            }

            return sum;
        }
    }
}