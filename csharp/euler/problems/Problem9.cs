using System;

namespace Euler
{
    public class Problem9 : Problem{

        public Problem9() : base(9, "Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.")
        {}

        protected override Int64 Solve(){
            int product = 0;
            double c = 0;
            int a = 1;
            int b = 1;
            bool found = false;

            for (a = 1; a < 500; a++)
            {
                for (b = 1; b < 500; b++)
                {
                    c = Math.Sqrt((double)((a*a) + (b*b)));
                    if (a + b + c == 1000)
                    {
                        found = true;
                        break;
                    }
                }

                if (found) break;
            }

            product = a * b * (int)c;

            return product;
        }
    }
}