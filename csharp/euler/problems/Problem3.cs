using System.Threading;
using System;
using System.Linq;

namespace Euler
{
    public class Problem3 : Problem{

        public Problem3() : base(3, "Find the largest prime factor of a composite number.")
        {}

        protected override Int64 Solve(){
            Int64 num = 600851475143;
            var factors = Primes.PrimeFactorise(num, 10000);
            // Since we identify factors from smallest to largest we can use the end of the list
            return factors.Last().Key;
        }
    }
}