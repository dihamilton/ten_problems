using System;

namespace Euler
{
    public class Problem2 : Problem{

        public Problem2() : base(2, "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million.")
        {}

        protected override Int64 Solve(){
            int a = 1;
            int b = 2;
            int c = 0;
            int sum = 2;  // Already seen one even number

            while (c < 1000000)
            {
                c = a + b;
                if (c % 2 == 0)
                {
                    sum += c;
                }
                a = b;
                b = c;
            }

            return sum;
        }
    }
}