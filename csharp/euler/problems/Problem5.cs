using System;
using System.Collections.Generic;

namespace Euler
{
    public class Problem5 : Problem{

        public Problem5() : base(5, "What is the smallest number divisible by each of the numbers 1 to 20?")
        {}

        protected override Int64 Solve(){
            int divisibleByAllLessThan = 20;

            var primeFactors = new Dictionary<int, int>();

            // To calculate the smallest number that is divisible by 1 - 20 we can multiply the highest power
            //  of all prime factors of the numbers from 1 - 20.

            //  As we discover prime factors with higher powers we will record those
            //  e.g. 2^1 will start of as our value, but 4 = 2^2, and then 8 = 2^3 finally 16 = 2^4 so we'll end up with 2^4 in our list.

            //  When working out the lowest number that's divisible from 1 - 20 we want to use the highest power for each prime factor
            //  because that guarantees it includes the number generated by that combination and also all others that divide it
            //  e.g. 32 is divisible by 16 (2^4) which means it's divisible by 2^3, 2^2 and 2^1 as well.
            for(int i = 1; i <= divisibleByAllLessThan; i++){
                var factors = Primes.PrimeFactorise(i, i/2+1);
                foreach(var factor in factors){
                    if(!primeFactors.ContainsKey(factor.Key)){
                        primeFactors[factor.Key] = factor.Value;
                    }
                    else if(primeFactors[factor.Key] < factor.Value){
                        primeFactors[factor.Key] = factor.Value;
                    }
                }
            }

            // Once we have all these prime factors and powers, we can multiple them together to get the smallest
            // number divisble by our starting set of numbers
            int smallestNumber = 1;
            foreach(var primeFactor in primeFactors){
                smallestNumber *= (int)Math.Pow(primeFactor.Key, primeFactor.Value);
            }

            return smallestNumber;
        }
    }
}