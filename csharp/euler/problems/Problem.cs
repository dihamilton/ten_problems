using System.Diagnostics;
using System;

namespace Euler
{

    public abstract class Problem
    {
        private int number = 0;
        private string description;
        private Int64 answer = 0;

        public double ElapsedMilliseconds { get; set; }
        
        public Problem(int number, string description){
            this.number = number;
            this.description = description;
        }

        public void Run(){
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            this.answer = Solve();
            stopwatch.Stop();
            this.ElapsedMilliseconds = ((double)stopwatch.ElapsedTicks / (double)Stopwatch.Frequency) * 1000.0;
        }

        public void Report(){

            Console.WriteLine(this.number + ": " + this.description);
            Console.WriteLine("Answer: " + this.answer + " in " + this.ElapsedMilliseconds + "ms");
        }

        // Child classes should override
        protected abstract Int64 Solve();

    };

}