<?php
require_once('problems/problem.php');
require_once('problems/problem1.php');
require_once('problems/problem2.php');
require_once('problems/problem3.php');
require_once('problems/problem4.php');
require_once('problems/problem5.php');
require_once('problems/problem6.php');
require_once('problems/problem7.php');
require_once('problems/problem8.php');
require_once('problems/problem9.php');
require_once('problems/problem10.php');

function main(){
    $problems = [
        new Problem1(),
        new Problem2(),
        new Problem3(),
        new Problem4(),
        new Problem5(),
        new Problem6(),
        new Problem7(),
        new Problem8(),
        new Problem9(),
        new Problem10()
    ];

    $totalElapsedMilliseconds = 0;

    foreach($problems as $problem){
        $problem->run();
        $problem->report();
        $totalElapsedMilliseconds += $problem->getElapsedMilliseconds();
    }
    print "PHP total elapsed: " . $totalElapsedMilliseconds . "ms\n";
}

main();

?>