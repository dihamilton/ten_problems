<?php

require_once('problem.php');

class Problem9 extends Problem
{

    public function __construct(){
        parent::__construct(9, 'Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.');
    }

    protected function solve(){
        $product = 0;
        $c = 0;
        $a = 1;
        $b = 1;
        $found = false;

        for ($a = 1; $a < 500; $a++)
        {
            for ($b = 1; $b < 500; $b++)
            {
                $c = sqrt((double)(($a*$a) + $b*$b));
                if ($a + $b + $c == 1000)
                {
                    $found = true;
                    break;
                }
            }

            if ($found) break;
        }

        $product = $a * $b * (int)$c;

        return $product;
    }
}
