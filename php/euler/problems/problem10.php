<?php

require_once('problem.php');
require_once(__DIR__ . '/../libraries/primes.php');

class Problem10 extends Problem
{

    public function __construct(){
        parent::__construct(10, 'Calculate the sum of all the primes below one million.');
    }

    protected function solve(){
        // Get all the primes below one million
        $primes = \Euler\Primes\Sieve(1000000);
            
        // Sum them up
        $sum = 0;
        foreach($primes as $prime){
            $sum += $prime;
        }

        return $sum;
    }
}
