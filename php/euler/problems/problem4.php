<?php

require_once('problem.php');
require_once(__DIR__ . '/../libraries/general_maths.php');

class Problem4 extends Problem
{

    public function __construct(){
        parent::__construct(4, 'Find the largest palindrome made from the product of two 3-digit numbers.');
    }

    protected function solve(){
        $low = 100;
        $high = 999;
        $num;
        $largestPalindrome = 0;

        for ($i = $high; $i >= $low; $i--)
        {
            for ($j = 990; $j >= $low; $j -= 11)
            {
                $num = $i * $j;
                if (Euler\Maths\isPalindrome($num))
                {
                    if ($num > $largestPalindrome)
                        $largestPalindrome = $num;
                }
            }
        }
        return $largestPalindrome;
    }
}
