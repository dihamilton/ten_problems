<?php

require_once('problem.php');

class Problem1 extends Problem
{

    public function __construct(){
        parent::__construct(1, 'Add all the natural numbers below 1000 that are multiples of 3 or 5.');
    }

    protected function solve(){
        $limit = 1000;
        $sum = 0; // sum of all the multiples

        for($i = 0; $i < $limit; $i++){
        
            if($i % 3 == 0 || $i % 5 == 0){
                $sum += $i;
            }
        }

        return $sum;
    }
}
