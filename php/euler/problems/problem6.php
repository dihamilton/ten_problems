<?php

require_once('problem.php');

class Problem6 extends Problem
{

    public function __construct(){
        parent::__construct(6, 'What is the difference between the sum of the squares and the square of the sums?');
    }

    protected function solve(){
        $difference = 0;
        $num = 100;

        $sumOfSquares = 0;
        $squareOfSum = 0;

        for ($i = 1; $i <= $num; $i++)
        {
            $sumOfSquares += $i*$i;
            $squareOfSum += $i;
        }

        $squareOfSum *= $squareOfSum;

        $difference = $squareOfSum - $sumOfSquares;

        return $difference;
    }
}
