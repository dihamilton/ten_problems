<?php

require_once('problem.php');
require_once(__DIR__ . '/../libraries/primes.php');

class Problem7 extends Problem
{

    public function __construct(){
        parent::__construct(7, 'Find the 10001st prime.');
    }

    protected function solve(){
        return \Euler\Primes\NthPrime(10001);
    }
}
