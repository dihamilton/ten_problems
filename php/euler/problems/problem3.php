<?php

require_once('problem.php');
require_once(__DIR__ . '/../libraries/primes.php');

class Problem3 extends Problem
{

    public function __construct(){
        parent::__construct(3, 'Find the largest prime factor of a composite number.');
    }

    protected function solve(){
        $num = 600851475143;
        $factors = Euler\Primes\primeFactorise($num, 10000);
        // Since we identify factors from smallest to largest we can use the end of the list
        return end(array_keys($factors));
    }
}
