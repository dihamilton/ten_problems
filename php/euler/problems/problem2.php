<?php

require_once('problem.php');

class Problem2 extends Problem
{

    public function __construct(){
        parent::__construct(2, 'Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million.');
    }

    protected function solve(){
        $a = 1;
        $b = 2;
        $c = 0;
        $sum = 2;  // Already seen one even number

        while ($c < 1000000)
        {
            $c = $a + $b;
            if ($c % 2 == 0)
            {
                $sum += $c;
            }
            $a = $b;
            $b = $c;
        }

        return $sum;
    }
}
