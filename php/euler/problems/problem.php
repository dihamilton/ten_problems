<?php

abstract class Problem
{
    private $number = 0;
    private $description = '';
    private $answer = 0;
    private $elapsedMilliseconds = 0;

    public function __construct($number, $description){
        $this->number = $number;
        $this->description = $description;
    }

    public function getElapsedMilliseconds(){
        return $this->elapsedMilliseconds;
    }
        
    public function run(){
        $start = microtime(true);
        $this->answer = $this->solve();
        $stop = microtime(true);
        $elapsedSeconds = $stop - $start;
        $this->elapsedMilliseconds = $elapsedSeconds * 1000.0;
    }

    public function report(){
        print(strval($this->number) . '. ' . $this->description . "\n");
        print('Answer: ' . strval($this->answer) . ' in ' . strval($this->elapsedMilliseconds) . 'ms' . "\n");
    }

    // Child classes should override solve
    // Note there is no specific language construct for enforcing it to be
    // implemented but polymorphism will still work
    abstract protected function solve();
}


?>