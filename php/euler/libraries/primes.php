<?php
namespace Euler\Primes;


// Sieve of Eratosthenes
// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
function sieve($n){

    $integers = array_fill(0, $n, false);

    $primes = array();
    // Create a list of odd integers from 3 up, because we already know 2 is the only even prime
    for($i = 3; $i < $n; $i += 2){
        $integers[$i] = true; // All these integers are possibly prime
    }
    // Since we're not including it above, let's add 2 to our list of known primes
    $primes[] = 2;

    $p = 3;
    $multipleOfP = 0;
    while($p*$p <= $n){    // We can stop looking when p² is greater than n

        $multipleOfP = $p*$p;  // Start from p² since smaller multiples of p will have already been marked
                            // e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                            // and even numbers have already been discounted

        // Strike out the multiples of p all the way up to n
        while($multipleOfP <= $n){
            $integers[$multipleOfP] = false;  // Since this number has at least one factor - p, it's not prime
            $multipleOfP += 2*$p;     // Go up in steps of 2p to avoid even numbers which are already out of the list
                                    // e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        }

        // Find the next number that isn't struck off 
        while($p <= $n){
            $p += 2;
            if($integers[$p] == true){
                break;
            }
        }
    }
    
    for($i = 0; $i < $n; $i++){
        if($integers[$i]){
            $primes[] = $i;
        }
    }

    return $primes;
}

function primeFactorise($n, $maxExpectedFactor)
{
    $primes = sieve($maxExpectedFactor);

    $factors = array();
    
    $factor = 0;
    $limit = $n/2;

    foreach($primes as $prime)
    {
        if($prime > $limit) break;
        $factor = $prime;

        // If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        // our prime divide the candidate so we can add it again.
        // E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        // and so on
        while($n % $factor == 0)
        {
            if(!array_key_exists($prime, $factors)){
                $factors[$prime] = 0;
            }
            $factors[$prime]++;
            $factor *= $prime;
        }

    }

    // If there are no factors then n is prime, which means it is divided only by itself once
    if(empty($factors)){
        $factors[$n] = 1;
    }

    return $factors;
}

function nthPrime($n){

    // Value of the nth prime is approximately n*ln(n)
    $m = 1.5 * $n;   // Fudge it high just in case
    $approxValue = (int)($m * log($m));
    // Generate sieve up to this value
    $primes = sieve($approxValue);
    return $primes[$n-1];
}
