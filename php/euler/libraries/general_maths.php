<?php
namespace Euler\Maths;

function toDigits($n)
{
    $digits = array();
    $temp = $n;
    $remainder = 0;
    while ($temp > 0)
    {
        $remainder = $temp % 10;
        $digits[] = $remainder;
        $temp = (int)($temp / 10);
    }
    return $digits;
}

function isPalindromeArray($digits)
{
    $size = count($digits);
    $halfway = (int)($size / 2);
    for ($i = 0; $i < $halfway; $i++)
    {
        if ($digits[$i] != $digits[($size - 1) - $i])
            return false;
    }

    return true;
}

function isPalindrome($n)
{
    return isPalindromeArray(toDigits($n));
}
