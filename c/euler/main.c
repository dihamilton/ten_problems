#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "problems/problem.h"
#include "problems/problem1.h"
#include "problems/problem2.h"
#include "problems/problem3.h"
#include "problems/problem4.h"
#include "problems/problem5.h"
#include "problems/problem6.h"
#include "problems/problem7.h"
#include "problems/problem8.h"
#include "problems/problem9.h"
#include "problems/problem10.h"



//double run_threaded(const vector<shared_ptr<Problem>> &problems);

int main()
{
    bool redact_answers = false;
    bool use_threads = false;
    int problemCount = 10;

    struct Problem* problems[11];
    for(int i = 0; i < 11; i++){
        problems[i] = NULL;
    }

    problems[1] = new_problem(1, "Add all the natural numbers below 1000 that are multiples of 3 or 5.", &solver1);
    problems[2] = new_problem(2, "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million.", &solver2);
    problems[3] = new_problem(3, "Find the largest prime factor of a composite number.", &solver3);
    problems[4] = new_problem(4, "Find the largest palindrome made from the product of two 3-digit numbers.", &solver4);
    problems[5] = new_problem(5, "What is the smallest number divisible by each of the numbers 1 to 20?", &solver5);
    problems[6] = new_problem(6, "What is the difference between the sum of the squares and the square of the sums?", &solver6);
    problems[7] = new_problem(7, "Find the 10001st prime.", &solver7);
    problems[8] = new_problem(8, "Find the greatest product of five consecutive digits in the 1000-digit number.", &solver8);
    problems[9] = new_problem(9, "Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.", &solver9);
    problems[10] = new_problem(10, "Calculate the sum of all the primes below one million.", &solver10);
    
    double total_elapsed_milliseconds = 0.0;
    double total_real_elapsed_milliseconds = 0.0;

    for(int i = 1; i <= problemCount; i++){
        if(!problems[i]){
            continue;
        }
        run(problems[i]);
        report(problems[i], redact_answers);
        total_elapsed_milliseconds += problems[i]->elapsed_milliseconds;
    }

    printf("C total elapsed: %fms\n", total_elapsed_milliseconds);

    // Cleanup memory
    for(int i = 1; i <= problemCount; i++){
        free_problem(problems[i]);
    }
    

    // if(use_threads){
    //     total_real_elapsed_milliseconds = run_threaded(problems);
    // }else{
    //     for(const auto &problem : problems){
    //         problem->run();
    //     }
    // }

    // for(const auto &problem : problems){
    //     problem->report(redact_answers);
    //     total_elapsed_milliseconds += problem->elapsed_milliseconds();
    // }

    // cout << "C++ Total elapsed: " << total_elapsed_milliseconds << "ms" << endl;
    // if(use_threads){
    //     cout << "C++ Total real time elapsed: " << total_real_elapsed_milliseconds << "ms" << endl;
    // }
    
    return 0;
}

// double run_threaded(const vector<shared_ptr<Problem>> &problems)
// {
//     vector<thread> threads;

//     Stopwatch stopwatch;
//     stopwatch.start();

//     for(const auto &problem : problems){
//         thread t([&] { problem->run(); });
//         threads.push_back(move(t));
//     }

//     for(auto &t : threads){
//         t.join();
//     }

//     stopwatch.stop();
//     return stopwatch.elapsed_milliseconds();
// }
