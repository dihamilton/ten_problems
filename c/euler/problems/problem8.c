#include "problem8.h"
#include <stdio.h>

int64_t solver8()
{
    FILE* in = fopen("/data/8.txt", "r");

    if(!in){
        printf("couldn't open data file");
        return 0;
    }
    
    int number_length = 1001;
    char number[1001];
    fgets (number, 1000, in);
    number[1000] = '\0';
    fclose(in);
    
    int largest_product = 0;
    int product = 0;

    // Go through and calculate all the 5 consecutive digit products in the number and return the largest found
    for (int i = 0; i < ((number_length - 1) - 4); i++){
        
        product = (number[i] - 48) * (number[i+1] - 48) * (number[i+2] - 48) * (number[i+3] - 48) * (number[i+4] - 48);

        if (product > largest_product){
            largest_product = product;
        }
    }

    return largest_product;
}