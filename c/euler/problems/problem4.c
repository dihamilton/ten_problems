#include "problem4.h"
#include "libraries/general_maths.h"


int64_t solver4()
{
    int low = 100;
    int high = 999;
    int num;
    int largestPalindrome = 0;

    for (int i = high; i >= low; i--)
    {
        for (int j = 990; j >= low; j -= 11)
        {
            num = i * j;
            if (is_palindrome(num))
            {
                if (num > largestPalindrome)
                    largestPalindrome = num;
            }
        }
    }
    return largestPalindrome;
}