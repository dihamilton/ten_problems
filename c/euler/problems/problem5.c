#include "problem5.h"
#include "libraries/primes.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>

int64_t solver5()
{
    int divisible_by_all_less_than = 20;
    int prime_factors_length = 20;
    prime_factor* prime_factors = malloc(sizeof(prime_factor) * prime_factors_length);
    memset(prime_factors, 0, sizeof(prime_factor) * prime_factors_length);

    // To calculate the smallest number that is divisible by 1 - 20 we can multiply the highest power
    // of all prime factors of the numbers from 1 - 20.

    // As we discover prime factors with higher powers we will record those
    // e.g. 2^1 will start of as our value, but 4 = 2^2, and then 8 = 2^3 finally 16 = 2^4 so we'll end up with 2^4 in our list.

    // When working out the lowest number that's divisible from 1 - 20 we want to use the highest power for each prime factor
    // because that guarantees it includes the number generated by that combination and also all others that divide it
    // e.g. 32 is divisible by 16 (2^4) which means it's divisible by 2^3, 2^2 and 2^1 as well.
    for(int i = 1; i <= divisible_by_all_less_than; i++){

        int max_expected_factor = i/2+1;
        int factors_length = 5;
        prime_factor* factors = malloc(sizeof(prime_factor) * factors_length);
        int max_factor = 0;
        prime_factorise(i, max_expected_factor, factors, &factors_length);
        for(int j = 0; j < factors_length; j++){
            int factor = factors[j].factor;
            int power = factors[j].power;
            // If we haven't seen this one, record it
            if(prime_factors[factor].factor == 0){
                prime_factors[factor].factor = factor;
                prime_factors[factor].power = power;
            }

            // And if we see one with a higher power we want to use that power instead
            if(prime_factors[factor].power < power){
                prime_factors[factor].power = power;
            }
        }

        free(factors);
        factors = NULL;
    }

    // Once we have all these prime factors and powers, we can multiple them together to get the smallest
    // number divisble by our starting set of numbers
    int smallest_number = 1;
    for(int i = 0; i < prime_factors_length; i++){
        if(prime_factors[i].factor != 0){
            smallest_number *= (int)pow(prime_factors[i].factor, prime_factors[i].power);
        }
    }
    
    free(prime_factors);
    prime_factors = NULL;

    return smallest_number;
}