#include "problem6.h"


int64_t solver6()
{
    int difference = 0;
    int num = 100;

    int sumOfSquares = 0;
    int squareOfSum = 0;

    for (int i = 1; i <= num; i++)
    {
        sumOfSquares += i*i;
        squareOfSum += i;
    }

    squareOfSum *= squareOfSum;

    difference = squareOfSum - sumOfSquares;

    return difference;
}