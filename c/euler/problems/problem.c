#include "problem.h"
#include "libraries/performance_timing.h"

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>


struct Problem* new_problem(int number, const char* description, solver s)
{
    struct Problem* p = malloc(sizeof(struct Problem));
    p->number = number;
    strncpy(p->description, description, 1024);
    p->s = s;
    return p;
}

double elapsed_milliseconds(struct Problem* p){
    return 0.0;
}

void run(struct Problem* p)
{
    double start = milliseconds_now();
    p->answer = p->s();
    double end = milliseconds_now();
    p->elapsed_milliseconds = end - start;
}

void report(struct Problem* p, bool redact_answers)
{
    printf("%d. %s\n", p->number, p->description);
    if(redact_answers){
        printf("Answer: REDACTED in %fms\n", p->elapsed_milliseconds);
    }else{
        printf("Answer: %" PRId64 " in %fms\n", p->answer, p->elapsed_milliseconds);
    }
}

void free_problem(struct Problem* p){
    free(p);
    p = NULL;
}

//#include "libraries/stopwatch.h"

//#include <iostream>

//using namespace std;
//using namespace euler::timing;

// Solve and print out result

// Problem::Problem(int number, const string &description) :
//     m_number(number), m_description(description){
// }

// int Problem::number(){
//     return m_number;
// }

// string Problem::description(){
//     return m_description;
// }

// int Problem::answer(){
//     return m_answer;
// }

// double Problem::elapsed_milliseconds(){
//     return m_elapsed_milliseconds;
// }

// void Problem::run(){
//     Stopwatch stopwatch;
//     stopwatch.start();
//     m_answer = solve();
//     stopwatch.stop();
//     m_elapsed_milliseconds = stopwatch.elapsed_milliseconds();
// }

// void Problem::report(bool redact_answer){
//     cout << to_string(m_number) << ". " << m_description << endl;
//     string answer = to_string(m_answer);
//     if(redact_answer){
//         answer = "REDACTED";
//     }
//     cout << "Answer: " << answer << " in " << m_elapsed_milliseconds << "ms" << endl;
// }