#include "problem1.h"

int64_t solver1()
{
    int64_t limit = 1000;
    int64_t sum = 0; // sum of all the multiples

    for (int64_t i = 0; i < limit; i++)
    {
        if (i % 3 == 0 || i % 5 == 0)
        {
            sum += i;
        }
    }

    return sum;
}