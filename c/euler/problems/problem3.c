#include "problem3.h"
#include "libraries/primes.h"
#include <stddef.h>
#include <stdlib.h>


int64_t solver3()
{
    int64_t num = 600851475143;
    int max_expected_factor = 10000;
    int factors_length = 10;
    prime_factor* factors = malloc(sizeof(prime_factor) * factors_length);
    int max_factor = 0;

    prime_factorise(num, max_expected_factor, factors, &factors_length);

    max_factor = factors[factors_length - 1].factor;

    free(factors);
    factors = NULL;

    return max_factor;
}