#include "problem9.h"
#include <math.h>

int64_t solver9()
{
    int product = 0;
    float c = 0;
    int a = 1;
    int b = 1;
    bool found = false;

    for (a = 1; a < 500; a++)
    {
        for (b = 1; b < 500; b++)
        {
            c = sqrt((float)((a*a) + (b*b)));
            if (a + b + c == 1000)
            {
                found = true;
                break;
            }
        }

        if (found) break;
    }

    product = a * b * (int)c;

    return product;
}