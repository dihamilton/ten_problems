#include "problem7.h"
#include "libraries/primes.h"

int64_t solver7()
{
    return nth_prime(10001);
}