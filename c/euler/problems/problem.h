#ifndef TEN_PROBLEMS_PROBLEM_H
#define TEN_PROBLEMS_PROBLEM_H
#include <stdint.h>
#include <stdbool.h>

typedef int64_t (*solver)();

struct Problem{
    int number;
    char description[1024];
    int64_t answer;
    double elapsed_milliseconds;
    solver s;
};

struct Problem* new_problem(int number, const char* description, solver s);

double elapsed_milliseconds(struct Problem* self);
void run(struct Problem* self);
void report(struct Problem* self, bool redact_answers);

void free_problem(struct Problem* p);

#endif