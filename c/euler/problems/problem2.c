#include "problem2.h"

int64_t solver2()
{
    int a = 1;
    int b = 2;
    int c = 0;
    int sum = 2;  // Already seen one even number

    while (c < 1000000)
    {
        c = a + b;
        if (c % 2 == 0)
        {
            sum += c;
        }
        a = b;
        b = c;
    }

    return sum;
}