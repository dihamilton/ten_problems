#ifndef TEN_PROBLEMS_PROBLEM1_H
#define TEN_PROBLEMS_PROBLEM1_H
#include <stdint.h>

#include "problem.h"

int64_t solver1();

#endif