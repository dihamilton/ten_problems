#include "problem10.h"

#include "libraries/primes.h"
#include <stdlib.h>
#include <math.h>

int64_t solver10()
{
    int n = 1000000;

    // Get the approximate number of primes below 1000000 so we know how much memory to allocate
    int approx_num_primes = (int)(n / log(n));
    int primes_length = approx_num_primes;
    int* primes = malloc(sizeof(int) * primes_length);
    int num_primes = 0;

    // Get all the primes below one million
    sieve(n, primes, primes_length, &num_primes);

     // Sum them up
    int64_t sum = 0;
    for(int i = 0; i <= num_primes; i++){
        sum += primes[i];
    }

    //free(primes);
    primes = NULL;

    return sum;
}