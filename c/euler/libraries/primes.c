
#include "primes.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

// Sieve of Eratosthenes
// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
void sieve(int n, int* primes, int primes_length, int* num_primes){

    memset(primes, 0, sizeof(int) * primes_length);

    int primesIndex = 0;
    bool* integers = malloc(sizeof(bool) * n);
    memset(integers, 0, sizeof(bool) * n);

    // Create a list of odd integers from 3 up, because we already know 2 is the only even prime
    for(int i = 3; i < n; i += 2){
        integers[i] = true; // All these integers are possibly prime
    }

    // Since we're not including it above, let's add 2 to our list of known primes
    primes[primesIndex] = 2;
    primesIndex++;

    int p = 3;
    int multipleOfP = 0;

    while(p*p <= n){    // We can stop looking when p² is greater than n

        multipleOfP = p*p;  // Start from p² since smaller multiples of p will have already been marked
                            // e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                            // and even numbers have already been discounted

        // Strike out the multiples of p all the way up to n
        while(multipleOfP <= n){
            integers[multipleOfP] = false;  // Since this number has at least one factor - p, it's not prime
            multipleOfP += 2*p;     // Go up in steps of 2p to avoid even numbers which are already out of the list
                                    // e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        }

        // Find the next number that isn't struck off 
        while(p <= n){
            p += 2;
            if(integers[p] == true){
                break;
            }
        }

    }

    for(int i = 0; i < n; i++){
        if(integers[i]){
            primes[primesIndex] = i;
            primesIndex++;
        }
    }

    *num_primes = primesIndex;

    free(integers);
    integers = NULL;
}


void prime_factorise(int64_t n, int max_expected_factor, prime_factor* factors, int* factors_length)
{
    memset(factors, 0, sizeof(prime_factor) * (*factors_length));
    int primes_length = max_expected_factor;
    int* primes = malloc(sizeof(int) * primes_length);
    int num_primes = 0;
    sieve(max_expected_factor, primes, primes_length, &num_primes);
    int64_t factor = 0;
    int64_t limit = n/2;
    int prime = 0;
    int factor_index = 0;

    for(int i = 0; i < num_primes; i++){
        
        prime = primes[i];
        if(prime > limit) break;
        factor = prime;

        // If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        // our prime divide the candidate so we can add it again.
        // E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        // and so on]
        if(n % factor == 0){
            factors[factor_index].factor = factor;
            factors[factor_index].power = 1;
            factor *= prime;
            while(n % factor == 0)
            {
                factors[factor_index].power++;
                factor *= prime;
            }
            factor_index++;
        }

    }
    *factors_length = factor_index;
    // If there are no factors then n is prime, which means it is divided only by itself once
    if(*factors_length == 0){
        factors[0].factor = n;
        factors[0].power = 1;
        *factors_length = 1;
    }
    free(primes);
    primes = NULL;
}



int nth_prime(int n){

    // Value of the nth prime is approximately n*ln(n)
    double m = 2 * n;
    int approx_value = (int)(m * log(m));
    int primes_length = approx_value;
    int num_primes = 0;
    int* primes = malloc(sizeof(int) * primes_length);
    // Generate sieve up to this value
    sieve(approx_value, primes, primes_length, &num_primes);
    int nth_prime = primes[n-1];
    free(primes);
    primes = NULL;
    return nth_prime;
}

