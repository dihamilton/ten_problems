#ifndef TEN_PROBLEMS_GENERAL_MATHS_H
#define TEN_PROBLEMS_GENERAL_MATHS_H

#include <stdbool.h>

void to_digits(int n, int* digits /* out */, int* length /* out */);
bool is_palindrome_array(int* n, int length);
bool is_palindrome(int n);

#endif