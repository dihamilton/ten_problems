
#include "performance_timing.h"
#include <time.h>

double milliseconds_now(){
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);
	double milliseconds = (now.tv_sec * 1000.0) + now.tv_nsec / 1000000.0;
	return milliseconds;
}