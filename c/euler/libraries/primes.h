#ifndef TEN_PROBLEMS_PRIMES_H
#define TEN_PROBLEMS_PRIMES_H

#include <inttypes.h>

typedef struct prime_factor{
    int64_t factor;
    int power;
} prime_factor;

void sieve(int n, int* primes, int primes_length, int* num_primes);
void prime_factorise(int64_t n, int max_expected_factor, prime_factor* factors, int* factors_length);
int nth_prime(int n);

#endif