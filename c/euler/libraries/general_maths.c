#include "general_maths.h"


#include <stddef.h>
#include <stdlib.h>

void to_digits(int n, int* digits, int* length /* out */)
{
    int temp = n;
    int rem = 0;
    int digit_index = 0;
    while (temp > 0)
    {
        rem = temp % 10;
        digits[digit_index] = rem;
        digit_index++;
        temp /= 10;
    }
    *length = digit_index;
}

bool is_palindrome_array(int* digits, int digits_length)
{
    int halfway = digits_length / 2;
    for (int i = 0; i < halfway; i++)
    {
        if (digits[i] != digits[(digits_length - 1) - i])
            return false;
    }

    return true;
}

bool is_palindrome(int n)
{
    int* digits = malloc(sizeof(int) * 256);  // Could figure out the length of the number in a loop first but other languages don't need to do that
    int digits_length = 0;
    to_digits(n, digits, &digits_length);
    bool result = is_palindrome_array(digits, digits_length);
    free(digits);
    digits = NULL;
    return result;
}
