import fileinput
import base64

def main():
    # base64 encoded so the answers aren't published in plaintext
    answers = ['MjMzMTY4Cg==', 'MTA4OTE1NAo=', 'Njg1Nwo=', 'OTA2NjA5Cg==', 'MjMyNzkyNTYwCg==', 'MjUxNjQxNTAK', 'MTA0NzQzCg==', 'NDA4MjQK', 'MzE4NzUwMDAK', 'Mzc1NTA0MDIwMjMK']
    problem_num = 1
    correct = True
    for line in fileinput.input():
        tokens = line.split(':')
        if len(tokens) > 1:
            if tokens[0] == 'Answer':
                details = tokens[1].split()
                answer = details[0]
                expected_answer = base64.b64decode(answers[problem_num - 1]).decode('utf-8').strip()
                if expected_answer != answer:
                    print("Problem " + str(problem_num) + " is incorrect: " + answer + " |" + expected_answer + "|")
                    correct = False
                problem_num += 1
            if 'total elapsed' in tokens[0]:
                details = tokens[0].split()
                language = details[0]
                elapsed = tokens[1].strip()
                elapsed = elapsed[:-2]

    if not correct or problem_num != 11:
        print(language + ' failed in ' + elapsed)
    else:
        #print(language + ' correct in ' + elapsed)
        print('"' + language + '":' + elapsed + ',')


if __name__ == "__main__":
    main()