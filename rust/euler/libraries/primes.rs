
use std::collections::HashMap;

// Sieve of Eratosthenes
// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
pub fn sieve(n: i64) -> Vec<i64> {
    //let mut integers = vec![0, n as usize]; // Note: why doesn't this method work with vectors of other types e.g. bool?
    let length = n as usize + 1;
    let mut integers: Vec<bool> = Vec::with_capacity(length);
    integers.resize(length, false);
    let mut primes: Vec<i64> = Vec::new();
    // Create a list of odd integers from 3 up, because we already know 2 is the only even prime
    let mut i: usize = 3;
    while i < length {
        integers[i] = true;
        i += 2;
    }
    // Since we're not including it above, let's add 2 to our list of known primes
    primes.push(2);

    let mut p = 3;
    let mut multiple_of_p;
    while p * p <= n {    // We can stop looking when p² is greater than n

        multiple_of_p = p*p;    // Start from p² since smaller multiples of p will have already been marked
                                // e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                                // and even numbers have already been discounted

        // Strike out the multiples of p all the way up to n
        while multiple_of_p <= n {
            integers[multiple_of_p as usize] = false;  // Since this number has at least one factor - p, it's not prime
            multiple_of_p += 2*p;   // Go up in steps of 2p to avoid even numbers which are already out of the list
                                    // e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        }

        // Find the next number that isn't struck off 
        while p <= n {
            p += 2;
            if integers[p as usize] == true {
                break;
            }
        }
    }
    
    for i in 0..n {
        if integers[i as usize] {
            primes.push(i);
        }
    }

    return primes;
}

pub fn prime_factorise(n: i64, max_expected_factor: i64) -> HashMap<i64, i64> 
{
    let primes = sieve(max_expected_factor);

    let mut factors = HashMap::new();
    
    let mut factor;
    let limit = n/2;

    for i in 0..primes.len() {

        let prime = primes[i as usize];
        if prime > limit { break; }
        factor = prime;

        // If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        // our prime divide the candidate so we can add it again.
        // E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        // and so on
        while n % factor == 0 {
            if !factors.contains_key(&prime) {
                factors.insert(prime,0);
            }
            factors.insert(prime, factors.get(&prime).unwrap() + 1);
            factor *= prime;
        }

    }

    // If there are no factors then n is prime, which means it is divided only by itself once
    if factors.len() == 0 {
        factors.insert(n, 1);
     }

    return factors;
}

pub fn nth_prime(n: i64) -> i64{

    // Value of the nth prime is approximately n*ln(n)
    let m = 1.5 * n as f64;   // Fudge it high just in case
    let approx_value = m * m.ln();
    // Generate sieve up to this value
    let primes = sieve(approx_value.floor() as i64);
    let index = (n - 1) as usize;
    return primes[index];
}
