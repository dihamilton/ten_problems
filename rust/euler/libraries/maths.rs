

fn to_digits(n: i32) -> Vec<i32>
{
    let mut digits: Vec<i32> = Vec::new();
    let mut temp = n;
    let mut rem;
    while temp > 0
    {
        rem = temp % 10;
        digits.push(rem);
        temp /= 10;
    }

    return digits;
}

fn is_palindrome_digits(digits : Vec<i32>) -> bool
{
    let size = digits.len();
    let halfway = size / 2;
    for i in 0..halfway {
        if digits[i] != digits[(size - 1) - i] {
            return false;
        }
    }
    return true;
}

pub fn is_palindrome(n: i32) -> bool
{
    return is_palindrome_digits(to_digits(n));
}