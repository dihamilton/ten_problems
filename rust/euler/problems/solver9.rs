pub mod solver;
// TODO: is there a bug here? If we fully qualify the trait in the impl statement this does not work,
// if we go with the use statement below it's fine
use solver::Solver;

pub struct Solver9 {}

impl Solver for Solver9 {

    fn number(&self) -> i32{
        return 9;
    }

    fn description(&self) -> String{
        return "Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.".to_string();
    }

    fn solve(&self) -> i64{
        let mut c = 0.0;
        let mut a = 1;
        let mut b = 1;
        let mut found = false;

        while a < 500 {
            b = 1;
            while b < 500 {
                c = (((a*a) + (b*b)) as f64).sqrt();
                // Compare as floats because if we round or truncate we get a false positive
                if a as f64 + b as f64 + c == 1000.0 {
                    found = true;
                    break;
                }
                b += 1;
            }

            if found {
                break;
            }
            a += 1;
        }
        let product = a * b * c.round() as i32 ;

        return product as i64;
    }
}
