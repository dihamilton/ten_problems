pub mod solver;
// TODO: is there a bug here? If we fully qualify the trait in the impl statement this does not work,
// if we go with the use statement below it's fine
use solver::Solver;

pub struct Solver6 {}

impl Solver for Solver6 {

    fn number(&self) -> i32{
        return 6;
    }

    fn description(&self) -> String{
        return "What is the difference between the sum of the squares and the square of the sums?".to_string();
    }

    fn solve(&self) -> i64{
        let num = 100;

        let mut sum_of_squares = 0;
        let mut square_of_sum = 0;

        for i in 1..=num {
            sum_of_squares += i*i;
            square_of_sum += i;
        }

        square_of_sum *= square_of_sum;

        let difference = square_of_sum - sum_of_squares;

        return difference as i64;
    }
}
