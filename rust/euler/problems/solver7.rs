pub mod solver;
// TODO: is there a bug here? If we fully qualify the trait in the impl statement this does not work,
// if we go with the use statement below it's fine
use solver::Solver;
#[path = "../libraries/primes.rs"] pub mod primes;

pub struct Solver7 {}

impl Solver for Solver7 {

    fn number(&self) -> i32{
        return 7;
    }

    fn description(&self) -> String{
        return "Find the 10001st prime.".to_string();
    }

    fn solve(&self) -> i64{
        return primes::nth_prime(10001);
    }
}
