pub mod solver;
use solver::Solver;
use std::time::{Instant};


pub struct Problem {
    answer: i64,
    elapsed_milliseconds: f64,
    solver: Box<dyn Solver>
}

impl Problem {

    pub fn new(solver: Box<dyn Solver>) -> Self{
        return Self{answer: 0, elapsed_milliseconds: 0.0, solver: solver};
    }
    
    pub fn elapsed_milliseconds(&self) -> f64{
        return self.elapsed_milliseconds;
    }

    fn answer(&self) -> i64{
        return self.answer;
    }

    // Note: Didn't need pub here when code was in the main file
    pub fn run(&mut self){
        let start = Instant::now();
        self.answer = self.solver.solve();
        let elapsed = start.elapsed().as_nanos();
        self.elapsed_milliseconds = (elapsed as f64)/1000000.0;
    }
    
    pub fn report(&self){
        println!("{0}. {1}", self.solver.number(), self.solver.description());
        println!("Answer: {0} in {1}ms", self.answer(), self.elapsed_milliseconds());
    }
}