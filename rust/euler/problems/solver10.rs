pub mod solver;
#[path = "../libraries/primes.rs"] pub mod primes;

// TODO: is there a bug here? If we fully qualify the trait in the impl statement this does not work,
// if we go with the use statement below it's fine
use solver::Solver;

pub struct Solver10 {}

impl Solver for Solver10 {

    fn number(&self) -> i32{
        return 10;
    }

    fn description(&self) -> String{
        return "Calculate the sum of all the primes below one million.".to_string();
    }

    fn solve(&self) -> i64{
        // Get all the primes below one million
        let primes = primes::sieve(1000000);
        
        // Sum them up
        let mut sum: i64 = 0;
        for prime in primes {
            sum += prime;
        }

        return sum;
    }
}
