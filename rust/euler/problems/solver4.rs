pub mod solver;
#[path = "../libraries/maths.rs"] pub mod maths;

use solver::Solver;

pub struct Solver4 {}

impl Solver for Solver4 {

    fn number(&self) -> i32{
        return 4;
    }

    fn description(&self) -> String{
        return "Find the largest palindrome made from the product of two 3-digit numbers.".to_string();
    }

    fn solve(&self) -> i64{
        let low: i32 = 100;
        let high: i32 = 999 + 1;
        let high_inner: i32 = 990 + 1;
        let mut num: i32;
        let mut largest_palindrome: i64 = 0;
        
        for i in (low..high).rev()
        {
            for j in (low..high_inner).rev().step_by(11)
            {
                num = i * j;
                if maths::is_palindrome(num)
                {
                    if num as i64 > largest_palindrome {
                        largest_palindrome = num as i64;
                    }
                }
            }
        }
        return largest_palindrome;
    }
}



