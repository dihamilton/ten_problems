pub mod solver;
// TODO: is there a bug here? If we fully qualify the trait in the impl statement this does not work,
// if we go with the use statement below it's fine
use solver::Solver;

pub struct Solver2 {}

impl Solver for Solver2 {

    fn number(&self) -> i32{
        return 2;
    }

    fn description(&self) -> String{
        return "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million.".to_string();
    }

    fn solve(&self) -> i64{
        let mut a = 1;
        let mut b = 2;
        let mut c = 0;
        let mut sum = 2;  // Already seen one even number

        while c < 1000000 {
            c = a + b;
            if c % 2 == 0{
                sum += c;
            }
            a = b;
            b = c;
        }

        return sum;
    }
}
