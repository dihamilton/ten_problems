
pub trait Solver {
    fn number(&self) -> i32;
    fn description(&self) -> String;
    fn solve(&self) -> i64;
}
