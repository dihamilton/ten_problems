pub mod solver;
// TODO: is there a bug here? If we fully qualify the trait in the impl statement this does not work,
// if we go with the use statement below it's fine
use solver::Solver;
use std::fs;
use std::collections::HashMap;

pub struct Solver8 {}

impl Solver for Solver8 {

    fn number(&self) -> i32{
        return 8;
    }

    fn description(&self) -> String{
        return "Find the greatest product of five consecutive digits in the 1000-digit number.".to_string();
    }

    fn solve(&self) -> i64{
        let contents = fs::read_to_string("/data/8.txt")
            .expect("Something went wrong reading the file");
        /*
        ifstream in("/data/8.txt");
        if(!in.is_open()) throw runtime_error("couldn't load 1000 digit number");
        string n;
        in >> n;
        */
        
        // Here we could use the property that the numeric value of a digit char is the ascii value minus 48 but this seems more obvious
        let digit_map: HashMap<char, i32> = [('0', 0),('1', 1),('2', 2),('3', 3),('4', 4),('5', 5),('6', 6),('7', 7),('8', 8),('9', 9)].iter().cloned().collect();
        
        let mut largest_product = 0;
        let mut product;
        let n: Vec<char> = contents.chars().collect();
        let length = contents.len();

        // Go through and calculate all the 5 consecutive digit products in the number and return the largest found
        for i in 0..(length - 4) {
            
            product = digit_map[&n[i]] * digit_map[&n[i+1]] * digit_map[&n[i+2]] * digit_map[&n[i+3]] * digit_map[&n[i+4]];

            if product > largest_product {
                 largest_product = product;
            }
        }

        return largest_product as i64;
    }
}
