pub mod solver;
#[path = "../libraries/primes.rs"] pub mod primes;

use solver::Solver;

pub struct Solver3 {}

impl Solver for Solver3 {

    fn number(&self) -> i32{
        return 3;
    }

    fn description(&self) -> String{
        return "Find the largest prime factor of a composite number.".to_string();
    }

    fn solve(&self) -> i64{
        let num : i64 = 600851475143;
        let factors = primes::prime_factorise(num, 10000);
        // Since we identify factors from smallest to largest we can use the end of the list
        let mut answer = 0;
        // TODO: Find a cleaner way to do this
        for (factor, _occurrences) in factors {
            if answer < factor {
                answer = factor;
            }
        }
        return answer;
    }
}



