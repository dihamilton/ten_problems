pub mod solver;
// TODO: is there a bug here? If we fully qualify the trait in the impl statement this does not work,
// if we go with the use statement below it's fine
use solver::Solver;

pub struct Solver1 {}

impl Solver for Solver1 {

    fn number(&self) -> i32{
        return 1;
    }

    fn description(&self) -> String{
        return "Add all the natural numbers below 1000 that are multiples of 3 or 5.".to_string();
    }

    fn solve(&self) -> i64{
        let limit = 1000;
        let mut sum = 0; // sum of all the multiples

        for i in 0..limit {
            if i % 3 == 0 || i % 5 == 0 {
                sum += i;
            }
        }

        return sum;
    }
}
