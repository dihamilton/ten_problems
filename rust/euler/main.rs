
#[path = "./problems/problem.rs"] pub mod problem;
#[path = "./problems/solver.rs"] pub mod solver;
#[path = "./problems/solver1.rs"] pub mod solver1;
#[path = "./problems/solver2.rs"] pub mod solver2;
#[path = "./problems/solver3.rs"] pub mod solver3;
#[path = "./problems/solver4.rs"] pub mod solver4;
#[path = "./problems/solver5.rs"] pub mod solver5;
#[path = "./problems/solver6.rs"] pub mod solver6;
#[path = "./problems/solver7.rs"] pub mod solver7;
#[path = "./problems/solver8.rs"] pub mod solver8;
#[path = "./problems/solver9.rs"] pub mod solver9;
#[path = "./problems/solver10.rs"] pub mod solver10;

use problem::Problem;


fn main() {

    let mut problems = [
        Problem::new(Box::new(solver1::Solver1{})),
        Problem::new(Box::new(solver2::Solver2{})),
        Problem::new(Box::new(solver3::Solver3{})),
        Problem::new(Box::new(solver4::Solver4{})),
        Problem::new(Box::new(solver5::Solver5{})),
        Problem::new(Box::new(solver6::Solver6{})),
        Problem::new(Box::new(solver7::Solver7{})),
        Problem::new(Box::new(solver8::Solver8{})),
        Problem::new(Box::new(solver9::Solver9{})),
        Problem::new(Box::new(solver10::Solver10{})),
    ];
    let mut total_elapsed_milliseconds = 0.0;
    for problem in problems.iter_mut() {
        problem.run();
        problem.report();
        total_elapsed_milliseconds += problem.elapsed_milliseconds();
    }

    println!("Rust total elapsed: {}ms", total_elapsed_milliseconds);
}