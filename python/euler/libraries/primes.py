import math
# Speedup attempt 1
#import numpy

# Speedup attempt 2
#import sympy

# Speedup attempt 3
#import primesieve

# Sieve of Eratosthenes
# https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
def sieve(n):
    #return list(sympy.primerange(0, n))    # Attempt 2
    #return primesieve.primes(n)            # Attempt 3

    #integers = numpy.zeros(n+1)               # Attempt 1
    integers = [False] * (n+1)      # Create an n sized array all containing False
    primes = []

    # Create a list of odd integers from 3 up, because we already know 2 is the only even prime
    for i in range(3, n, 2):
        integers[i] = True # All these integers are possibly prime

    # Since we're not including it above, let's add 2 to our list of known primes
    primes.append(2)

    p = 3
    multiple_of_p = 0
    while p*p <= n:    # We can stop looking when p² is greater than n

        multiple_of_p = p*p # Start from p² since smaller multiples of p will have already been marked
                            # e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                            # and even numbers have already been discounted

        # Strike out the multiples of p all the way up to n
        
        while multiple_of_p <= n:
            integers[multiple_of_p] = False     # Since this number has at least one factor - p, it's not prime
            multiple_of_p += 2*p                # Go up in steps of 2p to avoid even numbers which are already out of the list
                                                # e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        
        # Find the next number that isn't struck off 
        while p <= n:
            p += 2
            if integers[p] == True:
                break
        
    # Go through our array of composite (True) and prime (False) numbers and add all the primes to a list
    for i in range(0, n):
        if integers[i]:
            primes.append(i)
        
    return primes

def prime_factorise(n, max_expected_factor):

    primes = sieve(max_expected_factor)

    factors = {}        # Prime factors and how many times they occur
    factor = 0

    limit = n/2

    for prime in primes:

        if prime > limit:
            break
        factor = prime

        # If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        # our prime divide the candidate so we can add it again.
        # E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        # and so on
        while n % factor == 0:
            if prime not in factors:
                factors[prime] = 0
            factors[prime] += 1
            factor *= prime

    # If there are no factors then n is prime, which means it is divided only by itself once
    if len(factors) == 0:
        factors[n] = 1

    return factors



def nth_prime(n):

    # Value of the nth prime is approximately n*ln(n)
    m = 2 * n
    approx_value = m * math.log(m)
    # Generate sieve up to this value
    primes = sieve(int(approx_value))
    return primes[n-1]
