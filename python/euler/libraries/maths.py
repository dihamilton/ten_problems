
import math

def to_digits(n):
    digits = []
    temp = n
    rem = 0
    while temp > 0:
        rem = temp % 10
        digits.append(rem)
        temp //= 10     # division in python 3 defaults to floating point, use // syntax to do integer division instead

    return digits

def is_palindrome_array(digits):
    size = len(digits)
    halfway = size // 2     # division in python 3 defaults to floating point, use // syntax to do integer division instead
    for i in range (0, halfway):
        if digits[i] != digits[(size - 1) - i]:
            return False
    return True

def is_palindrome(n):
    return is_palindrome_array(to_digits(n))