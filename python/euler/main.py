
import sys
sys.path.insert(0, './problems')
import problem1
import problem2
import problem3
import problem4
import problem5
import problem6
import problem7
import problem8
import problem9
import problem10
#import yappi

def main():
    # Performance analysis
    #yappi.set_clock_type("cpu") # Use set_clock_type("wall") for wall time
    #yappi.start()
    problem_num = 0
    if len(sys.argv) > 1:
        problem_num = int(sys.argv[1])

    problems = []
    problems.append(problem1.Problem1())
    problems.append(problem2.Problem2())
    problems.append(problem3.Problem3())
    problems.append(problem4.Problem4())
    problems.append(problem5.Problem5())
    problems.append(problem6.Problem6())
    problems.append(problem7.Problem7())
    problems.append(problem8.Problem8())
    problems.append(problem9.Problem9())
    problems.append(problem10.Problem10())

    if problem_num != 0:
        problems[problem_num - 1].run()
        problems[problem_num - 1].report()
        return
    
    total_elapsed_milliseconds = 0
    for problem in problems:
        problem.run()
        problem.report()
        total_elapsed_milliseconds += problem.get_elapsed_milliseconds()

    print('Python total elapsed: ' + str(total_elapsed_milliseconds) + 'ms')
    #yappi.get_func_stats().print_all()

if __name__ == "__main__":
    main()