from problem import *
import sys
sys.path.insert(0, './libraries')
import primes
import maths

class Problem4(Problem):
    def __init__(self):
        super().__init__('4', 'Find the largest palindrome made from the product of two 3-digit numbers.')

    def solve(self):
        low = 100
        high = 999
        largest_palindrome = 0

        for i in range(high, low, -1):
            for j in range(990, low, -11):
                num = i * j
                if maths.is_palindrome(num):
                
                    if num > largest_palindrome:
                        largest_palindrome = num
                
        return largest_palindrome
