from problem import *
import sys
sys.path.insert(0, './libraries')
import primes

class Problem10(Problem):
    def __init__(self):
        super().__init__('10', 'Calculate the sum of all the primes below one million.')

    def solve(self):
        # Get all the primes below one million
        primes_array = primes.sieve(1000000)
            
        # Sum them up
        sum = 0
        for prime in primes_array:
            sum += prime

        return sum
