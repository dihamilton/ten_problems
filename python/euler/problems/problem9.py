from problem import *
import math

class Problem9(Problem):
    def __init__(self):
        super().__init__('9', 'Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000.')

    def solve(self):
        product = 0
        c = 0
        a = 1
        b = 1
        found = False

        for a in range(1, 500): #(a = 1 a < 500 a++)
            for b in range(1, 500): #(b = 1 b < 500 b++)
                c = math.sqrt((a*a) + (b*b))
                if a + b + c == 1000:
                    found = True
                    break

            if found: 
                break

        product = a * b * int(c)

        return product
