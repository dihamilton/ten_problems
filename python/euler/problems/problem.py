from abc import ABC, abstractmethod
import timeit

class Problem(ABC):
    def __init__(self, number, description):
        self.number = number
        self.description = description
        self.answer = 0
        self.elapsed_milliseconds = 0
    
    def get_number(self):
        return self.number

    def get_description(self):
        return self.description

    def get_answer(self):
        return self.answer

    def get_elapsed_milliseconds(self):
        return self.elapsed_milliseconds
        
    def run(self):
        start = timeit.default_timer()
        self.answer = self.solve()
        stop = timeit.default_timer()
        elapsed_seconds = stop - start
        self.elapsed_milliseconds = round(elapsed_seconds * 1000, 6)

    def report(self):
        print(str(self.number) + ". " + self.description)
        print("Answer: " + str(self.answer) + " in " + str(self.elapsed_milliseconds) + "ms")

    # Child classes should override
    @abstractmethod
    def solve(self):
        pass
