from problem import *

class Problem1(Problem):
    def __init__(self):
        super().__init__('1', 'Add all the natural numbers below 1000 that are multiples of 3 or 5.')

    def solve(self):
        limit = 1000
        sum = 0 # sum of all the multiples

        for i in range(limit):
        
            if i % 3 == 0 or i % 5 == 0:
                sum += i

        return sum
