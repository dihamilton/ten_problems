from problem import *

class Problem8(Problem):
    def __init__(self):
        super().__init__('8', 'Find the greatest product of five consecutive digits in the 1000-digit number.')

    def solve(self):

        with open("/data/8.txt", 'r') as f:
            n = f.read()

        # Here we could use the property that the numeric value of a digit char is the ascii value minus 48 but this seems more obvious
        digitMap = { '0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9 }
        
        largestProduct = 0
        product = 0

        length = len(n)
        # Go through and calculate all the 5 consecutive digit products in the number and return the largest found
        for i in range(0, length - 4): #i = 0 i < (length - 4) i++){
            
            product = digitMap[n[i]] * digitMap[n[i+1]] * digitMap[n[i+2]] * digitMap[n[i+3]] * digitMap[n[i+4]]

            if product > largestProduct:
                largestProduct = product

        return largestProduct
