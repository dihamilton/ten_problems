from problem import *
import sys
sys.path.insert(0, './libraries')
import primes

class Problem3(Problem):
    def __init__(self):
        super().__init__('3', 'Find the largest prime factor of a composite number.')

    def solve(self):
        num = 600851475143
        factors = primes.prime_factorise(num, 10000)
        # Since we identify factors from smallest to largest we can use the end of the list
        return list(factors)[-1]
