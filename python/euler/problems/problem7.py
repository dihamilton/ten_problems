from problem import *
import sys
sys.path.insert(0, './libraries')
import primes

# Naive solve.
# def is_prime(n):
#     half_n = n
#     if n == 2 or n == 3:
#         return True

#     for i in range(2, int(n/2) + 1):
#         if n%i == 0:
#             return False
#     return True

class Problem7(Problem):
    def __init__(self):
        super().__init__('7', 'Find the 10001st prime.')

    def solve(self):
        return primes.nth_prime(10001)

    # Naive solve.
    # def solve(self):
    #     prime_index = 10001
    #     current_prime_index = 0
    #     n = 1
    #     while current_prime_index < prime_index:
    #         n += 1
    #         # Generate 10001 primes
    #         if is_prime(n):
    #             current_prime_index += 1
            
    #     return n

