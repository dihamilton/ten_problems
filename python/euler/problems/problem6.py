from problem import *

class Problem6(Problem):
    def __init__(self):
        super().__init__('6', 'What is the difference between the sum of the squares and the square of the sums?')

    def solve(self):
        difference = 0
        num = 100

        sumOfSquares = 0
        squareOfSum = 0

        for i in range(1, (num + 1)):
            sumOfSquares += i*i
            squareOfSum += i

        squareOfSum *= squareOfSum

        difference = squareOfSum - sumOfSquares

        return difference