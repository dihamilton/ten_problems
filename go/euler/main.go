package main

import "fmt"
import "euler/problems"

func main() {

	allProblems := []*problems.Problem{
		problems.NewProblem(new(problems.Solver1)),
		problems.NewProblem(new(problems.Solver2)),
		problems.NewProblem(new(problems.Solver3)),
		problems.NewProblem(new(problems.Solver4)),
		problems.NewProblem(new(problems.Solver5)),
		problems.NewProblem(new(problems.Solver6)),
		problems.NewProblem(new(problems.Solver7)),
		problems.NewProblem(new(problems.Solver8)),
		problems.NewProblem(new(problems.Solver9)),
		problems.NewProblem(new(problems.Solver10)),
	}

	var totalElapsedMilliseconds float64 = 0

	for _, problem := range allProblems {
		problem.Run()
		problem.Report()
		totalElapsedMilliseconds += problem.ElapsedMilliseconds()
	}

	fmt.Println("Go total elapsed: " + fmt.Sprintf("%f", totalElapsedMilliseconds) + "ms")
}