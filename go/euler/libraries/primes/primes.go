package primes

import "math"

//import "fmt"

// Sieve of Eratosthenes
// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
func Sieve(n int) []int64 {

    integers := make([]bool, n + 1)
    var primes []int64
    // Create a list of odd integers from 3 up, because we already know 2 is the only even prime
    for i := 3; i < n; i += 2 {
        integers[i] = true; // All these integers are possibly prime
        //fmt.Println(i)
    }
    // Since we're not including it above, let's add 2 to our list of known primes
    primes = append(primes, 2)

    p := 3;
    multipleOfP := 0
    
    for p*p <= n {    // We can stop looking when p² is greater than n

        multipleOfP = p*p  // Start from p² since smaller multiples of p will have already been marked
                            // e.g. 7*2, 7*3, 7*4, 7*5, 7*6 will have been looked at when doing 3 & 5, 
                            // and even numbers have already been discounted

        // Strike out the multiples of p all the way up to n
        for multipleOfP <= n{
            integers[multipleOfP] = false;  // Since this number has at least one factor - p, it's not prime
            multipleOfP += 2*p;     // Go up in steps of 2p to avoid even numbers which are already out of the list
                                    // e.g. 7*7 -> 7*9 -> 7*11, no need to look at 7*8 and 7*10
        }

        // Find the next number that isn't struck off 
        for p <= n {
            p += 2;
            if integers[p] == true {
                break;
            }
        }
    }
    
    for i := range(integers) {
        if integers[i] == true {
            primes = append(primes, int64(i));
        }
    }

    return primes;
}

func PrimeFactorise(n int64, maxExpectedFactor int64) map[int64]int64 {
    primes := Sieve(int(maxExpectedFactor))

    var factors = make(map[int64]int64)
    var factor int64 = 0
    var limit int64 = n/2

    for i := range primes {
        prime := primes[i]
        if prime > limit {
            break
        } 
        factor = prime

        // If our candidate divides evenly into n then it's a factor, but also check whether successive powers of
        // our prime divide the candidate so we can add it again.
        // E.g. the prime factorisation of 12 is 2x2x3, so we need to identify that 12 % 2 == 0 and 12 % (2x2) == 0
        // and so on
        for n % factor == 0 {
            factors[prime] += 1 // Go maps return 0 if the key was not previously in the map
            factor = factor * prime
        }
    }

    // If there are no factors then n is prime, which means it is divided only by itself once
    if len(factors) == 0 {
        factors[n] = 1
     }

    return factors
}



func NthPrime(n int64) int64 {

    // Value of the nth prime is approximately n*ln(n)
    var m float64 = 2.0 * float64(n)
    approxValue := int(m * math.Log(m))
    // Generate sieve up to this value
    primes := Sieve(approxValue)
    return primes[n-1]
}