package maths

func ToDigits(n int) []int {

    var digits []int
    temp := n
    rem := 0
    for temp > 0 {
        rem = temp % 10
        digits = append(digits, rem)
        temp /= 10
    }

    return digits
}

func IsPalindromeSlice(digits []int) bool {
    size := len(digits)
    halfway := size / 2
    for i := 0; i < halfway; i++ {
        if digits[i] != digits[(size - 1) - i] {
            return false
        }
    }

    return true
}

func IsPalindrome(n int) bool {
    return IsPalindromeSlice(ToDigits(n));
}