package problems

type Solver1 struct {
}


func (s *Solver1) Number() int {
    return 1
}

func (s *Solver1) Description() string {
    return "Add all the natural numbers below 1000 that are multiples of 3 or 5."
}

func (s *Solver1) Solve() int64 {
    limit := 1000
    sum := 0; // sum of all the multiples

    for i := 0; i < limit; i++ {
        if (i % 3 == 0 || i % 5 == 0){
            sum += i
        }
    }

    return int64(sum)
}