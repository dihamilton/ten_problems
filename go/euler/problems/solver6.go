package problems


type Solver6 struct {
}


func (s *Solver6) Number() int {
	return 6
}

func (s *Solver6) Description() string {
	return "What is the difference between the sum of the squares and the square of the sums?"
}

func (s *Solver6) Solve() int64 {

	difference := 0
    num := 100

    sumOfSquares := 0
    squareOfSum := 0

    for i := 1; i <= num; i++{
        sumOfSquares += i*i
        squareOfSum += i
    }

    squareOfSum *= squareOfSum

    difference = squareOfSum - sumOfSquares

    return int64(difference)
}