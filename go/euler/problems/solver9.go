package problems

import "math"
//import "fmt"

type Solver9 struct {
}


func (s *Solver9) Number() int {
	return 9
}

func (s *Solver9) Description() string {
	return "Find the only Pythagorean triplet, {a, b, c}, for which a + b + c = 1000."
}

func (s *Solver9) Solve() int64 {

	var c float64 = 0
	a := 1
	b := 1
	found := false

	for a = 1; a < 500; a++ {
		for b = 1; b < 500; b++ {
			c = math.Sqrt(float64((a*a) + (b*b)))
			if float64(a + b) + c == 1000 {
				found = true
				break
			}
		}

		if found {
			break
		} 
	}

	// Interesting
	// i := 5
	// for i := 1; i < 3; i++ {
	// 	fmt.Print()
	// }

	// fmt.Println(i)

	product := float64(a * b) * c

	return int64(product)
}