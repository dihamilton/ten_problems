package problems

import "euler/libraries/maths"

type Solver4 struct {
}


func (s *Solver4) Number() int {
	return 4
}

func (s *Solver4) Description() string {
	return "Find the largest palindrome made from the product of two 3-digit numbers."
}

func (s *Solver4) Solve() int64 {

	low := 100
    high := 999
    num := 0
    largestPalindrome := 0

    for i := high; i >= low; i-- {
        for j := 990; j >= low; j -= 11 {
            num = i * j;
            if (maths.IsPalindrome(num)){
                if num > largestPalindrome {
					largestPalindrome = num
				}
            }
        }
    }
    return int64(largestPalindrome)
}