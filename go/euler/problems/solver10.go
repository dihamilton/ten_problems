package problems

import "euler/libraries/primes"

type Solver10 struct {
}


func (s *Solver10) Number() int {
    return 10
}

func (s *Solver10) Description() string {
    return "Calculate the sum of all the primes below one million."
}

func (s *Solver10) Solve() int64 {
    // Get all the primes below one million
    primes := primes.Sieve(1000000)

    // Sum them up
    var sum int64 = 0

    for _, prime := range (primes) {
        sum += int64(prime)
    }

    return sum
}