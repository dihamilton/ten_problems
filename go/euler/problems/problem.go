package problems

import "fmt"
import "strconv"
import "time"

type Problem struct {
    number int
    description string
    answer int64
    elapsedMilliseconds float64
    solver Solver
}


func NewProblem(solver Solver) *Problem {
    p := new(Problem)
    p.solver = solver
    p.number = solver.Number()
    p.description = solver.Description()
    return p
}

        
func (p *Problem) Run(){
    start := time.Now()
    p.answer = p.solver.Solve()
    elapsed := time.Since(start)
    p.elapsedMilliseconds = float64(elapsed.Nanoseconds()) / 1000000.0
}

func (p *Problem) Report(){
    fmt.Println(strconv.Itoa(p.number) + ". " + p.description)
    fmt.Println("Answer: " + strconv.FormatInt(p.answer, 10) + " in " + fmt.Sprintf("%f", p.elapsedMilliseconds) + "ms")
}

func (p *Problem) ElapsedMilliseconds() float64{
    return p.elapsedMilliseconds
}