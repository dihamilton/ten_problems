package problems

import "euler/libraries/primes"

type Solver3 struct {
}


func (s *Solver3) Number() int {
    return 3
}

func (s *Solver3) Description() string {
    return "Find the largest prime factor of a composite number."
}

func (s *Solver3) Solve() int64 {

    var num int64 = 600851475143
    factors := primes.PrimeFactorise(num, 10000)
    var answer int64 = 0
    // TODO: Find a cleaner way to do this
    for factor, _ := range factors {
        if answer < factor {
            answer = factor
        }
    }
        
    return answer;

}