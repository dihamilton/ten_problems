package problems

import "euler/libraries/primes"

type Solver7 struct {
}


func (s *Solver7) Number() int {
	return 7
}

func (s *Solver7) Description() string {
	return "Find the 10001st prime."
}

func (s *Solver7) Solve() int64 {
    return int64(primes.NthPrime(10001))
}