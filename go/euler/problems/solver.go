package problems

type Solver interface {
	Number() int
	Description() string
	Solve() int64
}