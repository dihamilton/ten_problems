package problems

type Solver2 struct {
}


func (s *Solver2) Number() int {
	return 2
}

func (s *Solver2) Description() string {
	return "Find the sum of all the even-valued terms in the Fibonacci sequence which do not exceed one million."
}

func (s *Solver2) Solve() int64 {
	a := 1
    b := 2
    c := 0
    sum := 2  // Already seen one even number

    for c < 1000000 {
        c = a + b
        if (c % 2 == 0){
            sum += c;
        }
        a = b
        b = c
    }

    return int64(sum)
}